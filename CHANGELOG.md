# Changelog
Describes notable changes to GroundCollapse between versions.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [0.1.1] - 2020-12-16
### Changed
 - Fixed a bug when deleting tiles that could cause them to still be referred to by other tiles, causing a crash.
 - Fixed a bug when renaming tiles that could cause them to still be referred to by other tiles, causing a crash.
 - Ensured constraints are listed in both tiles, so deleting and re-adding the tiles will not cause them to lose constraint info about each other.

## [0.1.0] - 2020-12-14
### Added
- Added a full graphical tileset editor! You can now build an entire tileset visually, rather than typing it into a .json file. Includes auto-slicing of sprite sheets, undo/redo support, copying tiles, and more. This makes it much faster to create a new tileset!
- Tiles can now have "tags" on their edges to set adjacency (essentially, categories of adjacency) - this means that if two tiles have the same tag on opposite edges, they can be next to each other on those edges.
- Tiles can now specify "prohibited" adjacencies - even if other rules (such as tags) would allow them to be adjacent, they will never be allowed to be if they have been set to prohibit that specific tile in that specific direction. This lets you set exceptions to tags.
- Added --aggregateStats option. If you set --aggregateStats=True, you will see a printout of performance stats from clingo.
- Added ability to specify adjacencies in all 8 directions in the tileset definition.
- Added ability to save resulting level chunks to disk. You can use --saveChunks=True to enable saving, or --saveHumanReadableChunks=True to save them in text format.
- Added option to set the save path with --baseChunkPath
- Added option to set the level name (for saving) with --levelName.
- Added option --makeUniqueLevelID, set it to True to add a random ID to the level name.
- Added ability for code to require or desire exits in particular directions when building a chunk.
- Added ability for code to specify required and desired walkability for specific parts of a chunk.
- Added ability to specify blocked exits from a tile in the tileset.
- Added ability to provide user-specified data that external code can retrieve from tiles in the tileset.
- Added more complex example tileset.
- Added constraints to help prevent neighboring chunks from being too similar to each other.

### Changed
- Improved style of wfc.pl to reduce duplication.
- Improved performance, reduced hitches while moving camera.
- Improved performance of chunk creation.
- Improved variety of output chunks.
- Improved order in which chunks are built to avoid creating chunks with no constrained edges.
- Fixed bug that caused some chunks to get temporarily duplicated into a second location.

## [0.0.1] - 2019-11-10
### Added
- First released version!
