from . import tileset
from pubsub import pub

def DefaultExtraDataFactory(extraData):
    return extraData

class TilesetLoader():
    def __init__(self, tileExtraDataFactory = DefaultExtraDataFactory):
        self.tileExtraDataFactory = tileExtraDataFactory

    def LoadTileset(self, tilesetPath, tileExtraDataFactory = None):
        tiles = self.CreateEmptyTileset(tilesetPath, tileExtraDataFactory)
        with open(tilesetPath, "r") as tilesetDataFile:
            tiles.Load(tilesetDataFile, tilesetPath)
        pub.sendMessage("tileset.loaded", tiles=tiles)
        return tiles

    def CreateEmptyTileset(self, tilesetPath, tileExtraDataFactory = None):
        if tileExtraDataFactory == None:
            tileExtraDataFactory = self.tileExtraDataFactory
        tiles = tileset.Tileset(tileExtraDataFactory)
        tiles.SetFilePath(tilesetPath)
        pub.sendMessage("tileset.loaded", tiles=tiles)
        return tiles

