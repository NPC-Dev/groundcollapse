import pandas
import os
import argparse
import json
import copy

class Tileset:
    def __init__(self, extraDataFactory, extraDataFormatter=None):
        self.tiles = {}
        self.extraDataFactory = extraDataFactory
        self.extraDataFormatter = None
        self.filePath = ""
        self.tileSize = Pos(32,32)

    def Load(self, file, filepath=""):
        self.filePath = filepath
        tilesetData = pandas.read_json(file)

        tilesetSpec = tilesetData.tileset

        self.tileSize = Pos.FromStr(tilesetSpec.tileSize)

        for tileName in tilesetSpec.tiles:
            self.tiles[tileName] = Tile(tileName, tilesetSpec.tiles[tileName], self.extraDataFactory)

        # Ensure that all adjacency allowed rules are symmetric. It wouldn't make sense for one tile to be allowed
        # next to another, but not vice versa!
        self.RecomputeAdjacencies()

    def RecomputeAdjacencies(self):
        for tile in self.tiles.values():
            tile.ClearComputedAdjacencies()

        for tile in self.tiles.values():
            self.SetupEdgeTagAdjacencies(tile)
            self.EnsureReciprocity(tile)

    def SetupEdgeTagAdjacencies(self, tile):
        tileName = tile.GetName()

        for direction in Dir.AllDirections():
            invDir = direction.Inverse()
            edgeTags = tile.GetEdgeTags(direction)
            if not edgeTags or len(edgeTags) == 0:
                continue

            for otherTileName, otherTile in self.tiles.items():
                if tile.CanBeAdjacent(otherTileName, direction):
                    continue

                otherEdgeTags = otherTile.GetEdgeTags(invDir)
                if not otherEdgeTags or len(otherEdgeTags) == 0:
                    continue
                
                if otherTile.IsAdjacencyProhibited(tileName, invDir) or \
                    tile.IsAdjacencyProhibited(otherTileName, direction):
                    continue

                if not edgeTags.isdisjoint(otherEdgeTags):
                    tile.AddComputedAdjacency(otherTileName, direction)
                    otherTile.AddComputedAdjacency(tileName, invDir)

    def SetFilePath(self, filePath):
        self.filePath = filePath

    def GetFilePath(self):
        return self.filePath

    def SetTileSize(self, x, y):
        if x < 0:
            x = 0
        
        if y < 0:
            y = 0

        self.tileSize.x = x
        self.tileSize.y = y

    def GetTileSize(self):
        return self.tileSize

    def GetTiles(self):
        return self.tiles

    def GetTile(self, tile):
        return self.tiles.get(tile, None)

    def CreateEmptyTile(self, tileName=None):
        if not tileName:
            newName = self.CreateUniqueTilename()
        elif tileName in self.tiles:
            return None
        else:
            newName = tileName

        self.tiles[newName] = Tile(newName, "", self.extraDataFactory)
        return self.tiles[newName]

    def AddTile(self, tile):
        self.tiles[tile.GetName()] = tile

        # TODO: Should we explicitly ensure reciprocity with explicit adjacencies?

        self.RecomputeAdjacencies()
        
    def CreateUniqueTilename(self):
        newID = 1
        while ("t%i" % newID) in self.tiles:
            newID += 1

        newName = "t%i" % newID
        return newName

    def RemoveTile(self, tileNameRemoved):
        removedTile = self.tiles.pop(tileNameRemoved)
        for tileName, tile in self.tiles.items():
            tile.NotifyTileRemoved(tileNameRemoved)

        return removedTile

    def DuplicateTile(self, origTile):
        newName = self.CreateUniqueTilename()
        newTile = copy.deepcopy(origTile)
        newTile.SetName(newName)
        self.tiles[newName] = newTile
        
        self.RecomputeAdjacencies()

        return newTile

    def RenameTile(self, tileToRename, oldName, newName):
        for tileName in self.tiles.keys():
            if tileName == newName:
                return False

        for tileName, tile in self.tiles.items():
            if tileName == oldName:
                tile.SetName(newName)

            tile.NotifyTileRenamed(oldName, newName)
    
        self.tiles[newName] = self.tiles.pop(oldName)
        
        return True

    def EnsureReciprocity(self, tile):
        # Ensures all adjacency constraints are symmetric (i.e., if a tile says it can be next
        # to another, the reverse is also true!) This reciprocity is technically redundant,
        # at least with the standard constraint program, because the default tile rules in wfc.pl
        # enforce this anyway, but it's just easier and nicer to edit the tileset if both tiles show
        # consistent rules.
        for direction in Dir.AllDirections():
            invDir = direction.Inverse()
            for otherTileName, otherTile in self.tiles.items():
                if otherTile.IsAdjacencyProhibited(tile.GetName(), invDir) or \
                    tile.IsAdjacencyProhibited(otherTile.GetName(), direction):
                    tile.ProhibitAdjacency(otherTileName, direction)
                    otherTile.ProhibitAdjacency(tile.GetName(), invDir)
                    continue
                elif tile.GetName() in otherTile.GetExplicitAdjacent(direction):
                    tile.AllowAdjacency(otherTileName, invDir)

            for otherTileName in tile.GetExplicitAdjacent(direction):
                otherTile = self.tiles[otherTileName]
                otherTile.AllowAdjacency(tile.GetName(), invDir)

    def ToJson(self, extraDataFormatter=None):
        tilesData = {}
        if not extraDataFormatter and self.extraDataFormatter:
            extraDataFormatter = self.extraDataFormatter
        for tileName, tile in self.tiles.items():
            tilesData[tile.GetName()] = tile.ToDict(extraDataFormatter)

        dictData = {"tileset" :
                        {
                            "tileSize" : self.tileSize.ToStr(),
                            "tiles" : tilesData
                        }
                    }
        return json.dumps(dictData, indent="\t")

    def __str__(self):
        result = "Tiles:\n" 
        for tile in self.tiles:
            result += "%s\n" % self.tiles[tile]
        return result

class Tile:
    def __init__(self, tileName, tileData, extraDataFactory):
        self.name = tileName

        self.path = ""
        self.pos = Pos(0,0)

        self.adjacent = {}
        self.neverAdjacent = {}
        self.computedAdjacent = {}
        self.edgeTags = {}
        self.canExit = {}
        
        self.extraData = None
        self.extraDataRaw = None
        
        if not tileData:
            return

        self.path = tileData["path"]

        self.pos = Pos.FromStr(tileData["pos"])
        
        if "adjacent" in tileData:
            for adjacentDir in tileData["adjacent"]:
                self.adjacent[Dir.FromStr(adjacentDir)] = set(tileData["adjacent"][adjacentDir])
        
        if "neverAdjacent" in tileData:
            for adjacentDir in tileData["neverAdjacent"]:
                self.neverAdjacent[Dir.FromStr(adjacentDir)] = set(tileData["neverAdjacent"][adjacentDir])

        if "edgeTags" in tileData:
            for adjacentDir in tileData["edgeTags"]:
                self.edgeTags[Dir.FromStr(adjacentDir)] = set(tileData["edgeTags"][adjacentDir])

        if "blockedExits" in tileData:
            for adjacentDir in tileData["blockedExits"]:
                self.canExit[Dir.FromStr(adjacentDir)] = False

        if "extradata" in tileData:
            self.extraDataRaw = tileData["extradata"]
            self.extraData = extraDataFactory(tileData["extradata"])

    def SetEdgeTags(self, direction, tags):
        self.edgeTags[direction] = set(tags)

    def SetName(self, name):
        self.name = name

    def GetName(self):
        return self.name

    def GetEdgeTags(self, dir):
        return self.edgeTags.get(dir, set())

    def IsAdjacencyProhibited(self, otherTile, dir):
        if len(self.neverAdjacent.get(dir, set())) == 0:
            return False
        return otherTile in self.neverAdjacent.get(dir, set())

    def GetAdjacent(self, dir):
        return (self.adjacent.get(dir, set()).union(self.computedAdjacent.get(dir, set()))).difference(self.neverAdjacent.get(dir, set()))
    
    def GetExplicitAdjacent(self, dir):
        return self.adjacent.get(dir, set())
    
    def ClearComputedAdjacencies(self):
        self.computedAdjacent.clear()

    def GetComputedAdjacent(self, dir):
        return self.computedAdjacent.get(dir, set())

    def CanBeAdjacent(self, tilename, direction):
        return tilename in self.GetAdjacent(direction)

    def AllowAdjacency(self, tileName, direction):
        if not direction in self.adjacent:
            self.adjacent[direction] = set()

        self.RemoveProhibitedAdjacency(tileName, direction)

        self.adjacent[direction].add(tileName)
    
    def AddComputedAdjacency(self, tileName, direction):
        if not direction in self.computedAdjacent:
            self.computedAdjacent[direction] = set()

        self.computedAdjacent[direction].add(tileName)

    def RemoveExplicitAdjacency(self, tileName, direction):
        if tileName in self.GetExplicitAdjacent(direction):
            self.adjacent[direction].remove(tileName)

    def RemoveProhibitedAdjacency(self, tileName, direction):
        if not self.IsAdjacencyProhibited(tileName, direction):
            return

        self.neverAdjacent[direction].remove(tileName)

    def ProhibitAdjacency(self, tileName, direction):
        if not direction in self.neverAdjacent:
            self.neverAdjacent[direction] = set()

        self.RemoveExplicitAdjacency(tileName, direction)
        
        if tileName in self.GetComputedAdjacent(direction):
            self.computedAdjacent[direction].remove(tileName)

        self.neverAdjacent[direction].add(tileName)

    def CanExit(self, dir):
        return self.canExit.get(dir, True)

    def SetCanExit(self, dir, canExit):
        self.canExit[dir] = canExit

    def GetExitDirData(self):
        return self.canExit

    def IsWalkable(self):
        # If we have blocked exits on all 8 directions, we are not walkable at all
        return len([dir for dir in self.canExit if self.canExit[dir] == False]) < 8

    def SetPos(self, x, y):
        if x < 0:
            x = 0
        
        if y < 0:
            y = 0

        self.pos = Pos(x, y)

    def GetPos(self):
        return self.pos

    def SetFile(self, path):
        self.path = path

    def GetFile(self):
        return self.path

    def GetExtraData(self):
        return self.extraData

    def ToDict(self, extraDataFormatter=None):
        adjacentDict = {}
        for direction, adjTiles in self.adjacent.items():
            tileList = list(adjTiles)
            tileList.sort()
            adjacentDict[direction.ToStr()] = tileList

        edgeTagsDict = {}
        for direction, edgeTags in self.edgeTags.items():
            tagList = list(edgeTags)
            tagList.sort()
            edgeTagsDict[direction.ToStr()] = tagList
        
        neverAdjacentDict = {}
        for direction, neverAdjacentTiles in self.neverAdjacent.items():
            tileList = list(neverAdjacentTiles)
            tileList.sort()
            neverAdjacentDict[direction.ToStr()] = tileList
        
        blockedExitDict = []
        for direction, canExit in self.canExit.items():
            if not canExit:
                blockedExitDict.append(direction.ToStr())

        extraDataDict = {}
        if None != extraDataFormatter:
            extraDataDict = extraDataFormatter(self.extraData)
        elif self.extraDataRaw:
            extraDataDict = self.extraDataRaw

        result = { "path" : self.path,
                "pos" : self.pos.ToStr()}

        if None != adjacentDict and len(adjacentDict) > 0:
            result["adjacent"] = adjacentDict

        if None != edgeTagsDict and len(edgeTagsDict) > 0:
            result["edgeTags"] = edgeTagsDict
        
        if None != neverAdjacentDict and len(neverAdjacentDict) > 0:
            result["neverAdjacent"] = neverAdjacentDict 
        
        if None != blockedExitDict and len(blockedExitDict) > 0:
            result["blockedExits"] = blockedExitDict

        if None != extraDataDict and len(extraDataDict) > 0:
            result["extradata"] = extraDataDict
    
        return result

    def NotifyTileRenamed(self, oldName, newName):
        for dir in self.adjacent.keys():
            self.adjacent[dir] = set([newName if currTile == oldName else currTile for currTile in self.adjacent[dir]])
            self.computedAdjacent[dir] = set([newName if currTile == oldName else currTile for currTile in self.computedAdjacent[dir]])
            self.neverAdjacent[dir] = set([newName if currTile == oldName else currTile for currTile in self.neverAdjacent[dir]])

        if self.extraData and hasattr(self.extraData, "NotifyTileRenamed"):
            self.extraData.NotifyTileRenamed(oldName, newName)

    def NotifyTileRemoved(self, tileName):
        for dir in self.adjacent.keys():
            if tileName in self.adjacent[dir]:
                self.adjacent[dir].remove(tileName)
            
        for dir in self.computedAdjacent.keys():
            if tileName in self.computedAdjacent[dir]:
                self.computedAdjacent[dir].remove(tileName)
        
        for dir in self.neverAdjacent.keys():
            if tileName in self.neverAdjacent[dir]:
                self.neverAdjacent[dir].remove(tileName)
        
        if self.extraData and hasattr(self.extraData, "NotifyTileRemoved"):
            self.extraData.NotifyTileRemoved(tileName)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "%s : %s" % (self.name, self.adjacent)

class Dir:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        assert(abs(self.x) < 2)
        assert(abs(self.y) < 2)

    def Inverse(self):
        return Dir(self.x * -1, self.y * -1)

    def AllDirections():
        return (Dir(-1,0), Dir(1,0), Dir(0,-1), Dir(0,1), Dir(-1,-1), Dir(-1,1), Dir(1,-1), Dir(1,1))

    def FromStr(dirData):
        xStr, yStr = dirData.split(",")
        return Dir.FromPosOffset(int(xStr), int(yStr))

    def ToStr(self):
        return "%s,%s" % (self.x, self.y)

    def FromPosOffset(x, y):
        x = int(x)
        if x != 0:
            x = int(x/abs(x))

        y = int(y)
        if y != 0:
            y = int(y/abs(y))

        return Dir(x,y)

    def __eq__(self, other):
        if not isinstance(other, Dir):
            return False
        
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return (self.x+2)*4 + (self.y+2)

    def __repr__(self):
        return "(%s,%s)" % (self.x, self.y)

class Pos:
    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)
        self.hash = (self.x,self.y).__hash__()

    def FromStr(posData):
        xStr, yStr = posData.split(",")
        return Pos(int(xStr),int(yStr))
    
    def ToStr(self):
        return "%s,%s" % (self.x, self.y)

    def __eq__(self, other):
        if not isinstance(other, Pos):
            return False
        
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return self.hash

    def __repr__(self):
        return "(%s,%s)" % (self.x, self.y)

if __name__ == "__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument('--path')

    args = argparser.parse_args()
    
    tileset = Tileset(None)
    with open(args.path, "r") as tilesetDataFile:
        tileset.Load(tilesetDataFile)
    
    print(tileset)
