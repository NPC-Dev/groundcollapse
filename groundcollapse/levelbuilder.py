#! /usr/bin/python

import sys
import os
from array import *
from . import tileset
import threading
import clingo
import copy
import threading
import json
import numbers

DIRS = (tileset.Dir(-1,0), tileset.Dir(1,0), tileset.Dir(0,-1), tileset.Dir(0,1))
EIGHT_DIRS = (tileset.Dir(-1,0), tileset.Dir(1,0), tileset.Dir(0,-1), tileset.Dir(0,1), tileset.Dir(-1,-1), tileset.Dir(-1,1), tileset.Dir(1,-1), tileset.Dir(1,1))

# The name of the final tile assignment symbol in the Clingo program 
TILE_ASSIGN_RESULT_NAME = "t"

class GroundingThread(threading.Thread):
    def __init__(self, clingoControl, programName, levelSize):
        threading.Thread.__init__(self)
        self.clingoControl = clingoControl
        self.programName = programName
        self.levelSize = levelSize

    def run(self):
        parts = [("base", []),
                (self.programName, [self.levelSize])]
        self.clingoControl.ground(parts)

class LevelBuilder:
    def __init__(self, tileset, clingoPath, clingoProgramName, levelSize = 4, seed = -1, randFreq = 0.5, numCores = -1, aggregateStats=False):
        self.programName = clingoProgramName
        self.levelSize = levelSize
        self.seed = seed
        self.randFreq = randFreq
        self.resultLevel = None
        
        self.aggregateStats = aggregateStats

        self.resultLevelLock = threading.Lock()

        self.isGrounded = False

        self.numCores = numCores
        if self.numCores == -1:
            self.numCores = os.cpu_count()-1

        self.tileset = tileset 
        self.clingoControl = self.LoadControl(clingoPath)

        self.handle = None

        self.groundingThread = None

        self.configurationLock = threading.Lock()
        self.nextBuildExternals = []
        self.currBuildExternals = []

        tilesetStatements = self.GenTilesetStatements()
        self.AugmentProgram(tilesetStatements)

    def Lock(self):
        return self.resultLevelLock

    def IsReadyToBuild(self):
        if self.IsBusy():
            return False
        
        if not self.resultLevelLock.acquire(blocking=False):
            return False

        try:
            if self.resultLevel == None:
                # If we have no result level, we are ready to start.
                return True
            else:
                return False
        finally:
            self.resultLevelLock.release()

    def LoadControl(self, clingoPath):
        clingoControl = clingo.Control(['-t %d' % self.numCores])

        clingoControl.configuration.solver.rand_freq = self.randFreq

        if self.seed != -1:
            clingoControl.configuration.solver.seed = self.seed

        clingoControl.configuration.solver.sign_def='rnd'
        clingoControl.configuration.solver.sign_def_disj='rnd'
        
        clingoControl.load(clingoPath)
        return clingoControl

    def GenTilesetStatements(self):
            extraProgramStatements = ""
            for tileName in self.tileset.GetTiles():
                tile = self.tileset.GetTile(tileName)
                tileStatement = "pattern(%s)." % tileName
                
                if tile.IsWalkable():
                    walkableStatement = "walkable(%s)." % tileName
                    extraProgramStatements += walkableStatement + "\n"
                
                for direction in self.EightDirs():
                    if tile.CanExit(direction):
                        walkableStatement = "can_exit(%s, (%i, %i))." % (tileName, direction.x, direction.y)
                        extraProgramStatements += walkableStatement + "\n"


                extraProgramStatements += tileStatement + "\n"
                
                for dir in self.EightDirs():
                    for adjacency in tile.GetAdjacent(dir):
                        tileAdjacencyStatement = "legal((DX, DY), P1, P2) :- DX == %d, DY == %d, pattern(P1), pattern(P2), P1 == %s, P2 == %s." % (dir.x, dir.y, tile.name, adjacency)
                        extraProgramStatements += tileAdjacencyStatement + "\n" 

            return extraProgramStatements

    def SetDebugMode(self):
        with self.configurationLock:
            fallbackStatement = clingo.parse_term("debugMode")
            self.nextBuildExternals.append(fallbackStatement)

    def SetFallbackMode(self):
        with self.configurationLock:
            fallbackStatement = clingo.parse_term("allowImperfect")
            self.nextBuildExternals.append(fallbackStatement)

    def AugmentProgram(self, extraProgramStatements):
        with self.clingoControl.builder() as builder:
            clingo.parse_program(extraProgramStatements, lambda stm : builder.add(stm))

    def GenPresetTileExternals(self, x, y, tileName):
        if tileName == "":
            return True
      
        with self.configurationLock:
            cellStatement = clingo.parse_term("cell((%d,%d))" % (x+1, y+1))
            assignStatement = clingo.parse_term("force((%d,%d),%s)" % (x+1, y+1, tileName))

            self.nextBuildExternals.append(cellStatement)
            self.nextBuildExternals.append(assignStatement)
        
            return True

    def GenMustBeWalkableExternal(self, x, y):
        with self.configurationLock:
            walkabilityStatement = clingo.parse_term("must_be_walkable((%d,%d))" % (x+1, y+1))
            self.nextBuildExternals.append(walkabilityStatement)

            return True
    
    def GenDesiredWalkableExternal(self, x, y):
        with self.configurationLock:
            walkabilityStatement = clingo.parse_term("desired_walkable((%d,%d))" % (x+1, y+1))
            self.nextBuildExternals.append(walkabilityStatement)

            return True
    
    def GenRequiredExitExternal(self, x, y, dirX, dirY):
        with self.configurationLock:
            walkabilityStatement = clingo.parse_term("require_exit((%d,%d),(%d,%d))" % (x+1, y+1, dirX, dirY))
            self.nextBuildExternals.append(walkabilityStatement)

            return True
    
    def GenDesiredExitExternal(self, x, y, dirX, dirY):
        with self.configurationLock:
            walkabilityStatement = clingo.parse_term("desire_exit((%d,%d),(%d,%d))" % (x+1, y+1, dirX, dirY))
            self.nextBuildExternals.append(walkabilityStatement)

            return True

    def GenUndesirableExternal(self, x, y, pattern):
        with self.configurationLock:
            undesirableStatement = clingo.parse_term("undesirable((%d,%d),%s)" % (x+1, y+1, pattern))
            self.nextBuildExternals.append(undesirableStatement)

            return True


    def ApplyExternals(self):
        if self.IsBusy():
            return False

        with self.configurationLock:
            for external in self.nextBuildExternals:
                self.clingoControl.assign_external(external, True)
            
            self.currBuildExternals = self.nextBuildExternals.copy()
            self.nextBuildExternals = []
            return True

    def Dirs(self):
        return DIRS

    def EightDirs(self):
        return EIGHT_DIRS

    def IsGrounding(self):
        return self.groundingThread and self.groundingThread.isAlive()

    def Ground(self):
        if self.IsBusy():
            return False
        
        self.groundingThread = GroundingThread(self.clingoControl, self.programName, self.levelSize)
        self.groundingThread.start()
        return True

    def StartBuild(self):
        if not self.IsReadyToBuild():
            return False
      
        if not self.isGrounded:
            self.Ground()
        else:
            self.Solve()
        return True

    def IsGrounded(self):
        return self.isGrounded

    def Solve(self):
        if not self.IsGrounded:
            return
        
        if self.IsBusy():
            return
        
        self.ApplyExternals()

        self.handle = self.clingoControl.solve(on_model=self.StoreResult, async_=True)

    def Interrupt(self):
        if self.IsGrounding():
            self.groundingThread.join()
            self.groundingThread = None
            
        if self.handle:
            self.clingoControl.interrupt()

    def Tick(self):
        if self.handle:
            if self.handle.wait(0):
                self.handle = None

        if self.groundingThread != None and not self.groundingThread.isAlive():
            # Grounding finished!
            self.isGrounded = True
            self.groundingThread = None
            self.Solve()
        
    def StoreResultSymbols(self, symbols):
        with self.resultLevelLock:

            self.resultLevel = [[""] * self.levelSize for i in range(self.levelSize)]
            for symbol in symbols:
                if symbol.name == TILE_ASSIGN_RESULT_NAME:
                    # Result is in the form [(X,Y),TILE]
                    posStr = symbol.arguments[0]

                    # Extract the x and y from the first argument tuple
                    xPos = int(posStr.arguments[0].number-1)
                    yPos = int(posStr.arguments[1].number-1)

                    # Extract the tile name from the second argument
                    assignment = symbol.arguments[1].name

                    # This tile is outside the level bounds, probably from a pre-set constraint
                    if (xPos < 0 or xPos > self.levelSize - 1
                        or yPos < 0 or yPos > self.levelSize - 1):
                            continue
                    
                    self.resultLevel[xPos][yPos] = assignment

    def StoreResult(self, model):
        self.StoreResultSymbols(model.symbols(shown=True))

    def PrintResult(self):
        with self.resultLevelLock:
            print(self.resultLevel)

    def GetTileset(self):
        return self.tileset

    def GetResultLevel(self):
        with self.resultLevelLock:
            return copy.deepcopy(self.resultLevel)
    
    def IsBusy(self):
        return self.IsGrounding() or self.handle

    def Reset(self):
        if self.IsGrounding() or self.handle:
            return False
       
        if self.aggregateStats:
            self.AvgStats()

        with self.configurationLock:
            for external in self.currBuildExternals:
                self.clingoControl.assign_external(external, False)
          
            self.clingoControl.cleanup()

            self.currBuildExternals = []

        with self.resultLevelLock:
            self.resultLevel = None
            return True

    def TryGetStats(self):
        with self.resultLevelLock:
            with self.configurationLock:
                try:
                    stats = self.clingoControl.statistics
                    return stats
                except Exception as e:
                    # Do nothing.
                    return None
    
    def AvgStats(self):
        stats = self.TryGetStats()
        if stats:
            if not hasattr(self,"avgStats"):
                self.avgStats = stats
                self.numStats = 1

            self.avgStats = self.AvgDicts(stats, self.avgStats, self.numStats)
            self.numStats += 1

    
    def AvgDicts(self, stats, avgStats, numStats):
        # Average all values in a nested dictionary into a previous set of values.
        for key in stats:
            if isinstance(stats[key], numbers.Number):
                if not key in avgStats:
                    avgStats[key] = 0
                avgVal = avgStats[key]
                avgStats[key] = (stats[key] + numStats * avgVal)/(numStats+1)
            elif isinstance(stats[key], dict):
                if not key in avgStats:
                    avgStats[key] = {}
                avgStats[key] = self.AvgDicts(stats[key], avgStats[key], numStats)
            elif isinstance(stats[key], list):
                for i in range(0, len(stats[key])):
                    if i >= len(avgStats[key]):
                        avgStats[key].append(0)
                    avgVal = avgStats[key][i]
                    avgStats[key][i] = (stats[key][i] + numStats * avgVal)/(numStats+1)

        return avgStats

    def PrintAvgStats(self):
        statsString = json.dumps(self.avgStats, sort_keys=True, indent=4, separators=(',', ': '))
        print("Stats: " + statsString)
    
    def PrintStats(self):
        stats = self.TryGetStats()
        if stats:
            statsString = json.dumps(stats, sort_keys=True, indent=4, separators=(',', ': '))
            print("Stats: " + statsString)
