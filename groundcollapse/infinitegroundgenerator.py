from groundcollapse import levelbuilder
from groundcollapse import levelstore
from groundcollapse import tileset
from groundcollapse import queueupdaterthread
from groundcollapse import queuesorterthread
from groundcollapse import buildqueue
from groundcollapse import cameraholder
import pygame
import threading

class InfiniteGroundGenerator:
    def __init__(self, 
                 cameraHolder, 
                 levelStore, 
                 builder, 
                 debugOnFallback, 
                 chunkSize,
                 worldBuildScreens, 
                 fallbackOptimizeSeconds, 
                 optimizationSeconds,
                 failureSeconds,
                 optimizationScreens):
        
        self.cameraHolder = cameraHolder
        self.levelStore = levelStore
        self.builder = builder
        self.builderPrepped = True
        self.builderReady = False

        self.debugOnFallback = debugOnFallback
        self.chunkSize = chunkSize
        self.worldBuildScreens = worldBuildScreens
        self.fallbackOptimizeSeconds = fallbackOptimizeSeconds
        self.optimizationSeconds = optimizationSeconds
        self.failureSeconds = failureSeconds
        self.optimizationScreens = optimizationScreens

        self.timeSinceChunkQueueUpdate = 0.0
        self.timeSinceBuildStart = 0.0
        self.timeSinceBuildEnd = 0.0
        
        self.lastTick = 0.0

        self.fallbackMode = False
        self.chunkQueueThread = self.UpdateChunkQueue()
        self.chunkQueue = buildqueue.BuildQueue(self.cameraHolder, self.levelStore, self.chunkSize)
        
        self.pos = tileset.Pos(0,0)
        if self.cameraHolder:
            self.pos = self.cameraHolder.GetCameraCenter()

    def ResetBuilderForNewChunk(self, builder):
        return builder.Reset()

    def MakeChunkConstraints(self, pos):
        for xDiff in range(-1,2):
            for yDiff in range(-1,2):
                if xDiff == 0 and yDiff == 0:
                    continue
                
                neighborPos = pygame.Vector2(pos.x + self.chunkSize.x * xDiff, pos.y + self.chunkSize.y * yDiff)
                self.PreventSimilarityToChunk(neighborPos)

        return self.MakeEdgeConstraints(pos)

    def MakeEdgeConstraints(self, pos):
        neighborChunkX = pos.x - self.chunkSize.x
        neighborChunk = self.levelStore.GetTiles(neighborChunkX, pos.y)
        if len(neighborChunk) > 0:
            for y in range(0, len(neighborChunk[0])):
                self.builder.GenPresetTileExternals(-1, y, neighborChunk[len(neighborChunk)-1][y])

        neighborChunkX = pos.x + self.chunkSize.x
        neighborChunk = self.levelStore.GetTiles(neighborChunkX, pos.y)
        if len(neighborChunk) > 0:
            for y in range(0, len(neighborChunk[0])):
                self.builder.GenPresetTileExternals(self.chunkSize.x, y, neighborChunk[0][y])
            
        neighborChunkY = pos.y - self.chunkSize.y
        neighborChunk = self.levelStore.GetTiles(pos.x, neighborChunkY)
        if len(neighborChunk) > 0:
            for x in range(0, len(neighborChunk)):
                self.builder.GenPresetTileExternals(x, -1, neighborChunk[x][len(neighborChunk[0])-1])

        neighborChunkY = pos.y + self.chunkSize.y
        neighborChunk = self.levelStore.GetTiles(pos.x, neighborChunkY)
        if len(neighborChunk) > 0:
            for x in range(0, len(neighborChunk)):
                self.builder.GenPresetTileExternals(x, self.chunkSize.y, neighborChunk[x][0])

        return True

    def PreventSimilarityToChunk(self, chunk):
        chunk = self.levelStore.GetTiles(self.pos.x, self.pos.y)
        if len(chunk) == 0:
            return

        for x in range(0, len(chunk[0])):
            for y in range(0, len(chunk[1])):
                self.builder.GenUndesirableExternal(x, y, chunk[x][y])

    def UpdateChunkQueue(self):
        chunkQueueThread = queueupdaterthread.QueueUpdaterThread(self.cameraHolder.GetCameraRect(), self.levelStore, self.chunkSize, self.worldBuildScreens)
        chunkQueueThread.start()
        return chunkQueueThread

    def SortChunkQueue(self):
        chunkQueueThread = queuesorterthread.QueueSorterThread(self.cameraHolder.GetCameraRect(), self.chunkQueue)
        chunkQueueThread.start()
        return chunkQueueThread

    def ShouldAllowOptimizeTime(self, chunkRect):
        if self.optimizationScreens == 0:
            return True

        cameraRect = pygame.Rect(self.cameraHolder.GetCameraRect())
        if self.optimizationScreens < 1:
            cameraRect.inflate_ip(-cameraRect.width * (1.0-self.optimizationScreens), -cameraRect.height * (1.0-self.optimizationScreens))
        else:
            cameraRect.inflate_ip(cameraRect.width * self.optimizationScreens, cameraRect.height * self.optimizationScreens)

        return not cameraRect.colliderect(chunkRect)

    def Tick(self, tickDiff = None):
        self.builder.Tick()

        if not tickDiff:
            newTime = pygame.time.get_ticks()
            tickDiff = (newTime - self.lastTick)/1000.0
            self.lastTick = newTime
        
        self.timeSinceChunkQueueUpdate += tickDiff

        resultLevel = self.builder.GetResultLevel()

        if self.chunkQueueThread != None and not self.chunkQueueThread.is_alive():
            self.chunkQueue = self.chunkQueueThread.GetQueue()
            self.chunkQueueThread = None
        elif self.timeSinceChunkQueueUpdate > 1.0 and self.chunkQueueThread == None:
            self.timeSinceChunkQueueUpdate = 0.0
            self.chunkQueueThread = self.UpdateChunkQueue()

        if not self.builderReady and self.builder.IsGrounded():
            self.timeSinceBuildStart += tickDiff

        if None != resultLevel:
            self.timeSinceBuildEnd += tickDiff
            self.levelStore.StoreTiles(self.pos.x, self.pos.y, resultLevel)

            if not self.builderReady and self.chunkQueue.Size() > 0:
                if self.fallbackMode and self.timeSinceBuildEnd < self.fallbackOptimizeSeconds:
                    # Give fallback mode longer to refine.
                    return
                elif self.ShouldAllowOptimizeTime(pygame.Rect(self.pos.x,self.pos.y,len(resultLevel),len(resultLevel[0]))) \
                        and self.timeSinceBuildEnd < self.optimizationSeconds:
                    # Give chunks generated offscreen a bit of time to optimize before finalizing them
                    return

                self.builder.Interrupt()

                if not self.ResetBuilderForNewChunk(self.builder):
                    return

                if self.chunkQueueThread == None:
                    self.chunkQueueThread = self.SortChunkQueue()

                self.timeSinceBuildEnd = 0.0
                self.builderReady = True
                self.fallbackMode = False
        elif not self.fallbackMode and not self.builderReady and self.timeSinceBuildStart > self.failureSeconds:
            self.builder.Interrupt()

            if not self.ResetBuilderForNewChunk(self.builder):
                return

            if self.debugOnFallback:
                print("Build failed - entering debug mode: " + str(self.pos.x) + ", " + str(self.pos.y))
                self.builder.SetDebugMode()
            else:
                print("Build failed - entering fallback mode: " + str(self.pos.x) + ", " + str(self.pos.y))
                self.builder.SetFallbackMode()
            self.fallbackMode = True

            if not self.MakeChunkConstraints(self.pos):
                return

            self.builderPrepped = True

        if self.builderReady and not self.builderPrepped and self.chunkQueueThread == None and self.chunkQueue.Size() > 0:
            self.pos = self.chunkQueue.Pop()
            while (self.levelStore.Contains(self.pos.x, self.pos.y)):
                if self.chunkQueue.Size() == 0:
                    self.pos = None
                    break
                self.pos = self.chunkQueue.Pop()

            if self.pos == None:
                return

            if not self.MakeChunkConstraints(self.pos):
                return

            self.builderPrepped = True

        if self.builderPrepped and self.builder.StartBuild():
            self.builderReady = False
            self.builderPrepped = False
            self.timeSinceBuildStart = 0.0


