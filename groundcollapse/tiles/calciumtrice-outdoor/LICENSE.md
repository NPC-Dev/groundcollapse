Outdoor Tileset by Calciumtrice, licensed under Creative Commons Attribution 3.0 license: https://creativecommons.org/licenses/by/3.0/

https://opengameart.org/content/outdoor-tileset

This tileset has been modified from its original version to provide alternate color variants.
