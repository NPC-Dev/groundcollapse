#! /usr/bin/python

from . import tileset
import pygame
import threading
from pubsub import pub

'''
A storage structure for level data.

Only keeps maxLoadedRegion space loaded at a time.

This implementation currently uses a naive dictionary storage and just
throws out any unloaded objects permanently.

Other implementations could:
- Save and load data from disk
- Optimize speed with binary space partitioning (e.g. quadtrees)

'''
class LevelStore:

    def __init__(self,
                    maxLoadedRegion,
                    cameraHolder):
        self.maxLoadedRegion = maxLoadedRegion
        self.chunks = {}
        self.minLoadedCoords = pygame.Vector2(0,0)
        self.maxLoadedCoords = pygame.Vector2(0,0)
        self.cameraHolder = cameraHolder
        self.chunksLock = threading.Lock()

    def StoreTiles(self, x, y, tiles):
        self.StoreTilesAt(tileset.Pos(x,y), tiles)

    def StoreTilesAt(self, pos, tiles):
        if tiles == None:
            self.RemoveTilesAt(pos)
            return

        with self.chunksLock:
            if pos in self.chunks:
                pub.sendMessage("tilegen.deleted", chunkPos=pos, tiles=self.chunks[pos])
            self.chunks[pos] = tiles
            
        pub.sendMessage("tilegen.created", chunkPos=pos, tiles=tiles)
        
        self.minLoadedCoords.x = min(self.minLoadedCoords.x, pos.x)
        self.minLoadedCoords.y = min(self.minLoadedCoords.y, pos.y)
        self.maxLoadedCoords.x = max(self.maxLoadedCoords.x, pos.x + len(tiles))
        self.maxLoadedCoords.y = max(self.maxLoadedCoords.y, pos.y + len(tiles[0]))

        cameraCenter = self.cameraHolder.GetCameraCenter()
        self.Prune(cameraCenter.x, cameraCenter.y)
   
    def RemoveTiles(self, x, y):
        self.RemoveTilesAt(tileset.Pos(x,y))

    def RemoveTilesAt(self, pos):
        with self.chunksLock:
            tiles = self.chunks.pop(pos,[])
        
            pub.sendMessage("tilegen.deleted", chunkPos=pos, tiles=tiles)

    def GetTilesAt(self, pos):
        with self.chunksLock:
            return self.chunks.get(pos, [])

    def GetTiles(self, x, y):
        return self.GetTilesAt(tileset.Pos(x,y))

    def Contains(self, x, y):
        return self.ContainsAt(tileset.Pos(x,y))

    def ContainsAt(self, pos):
        return pos in self.chunks

    def Clear(self):
        for (pos, tiles) in self.chunks.items():
            pub.sendMessage("tilegen.deleted", chunkPos=pos, tiles=tiles)

        with self.chunksLock:
            self.chunks.clear()

    def Prune(self, x, y):
        extents = self.maxLoadedCoords - self.minLoadedCoords
        if extents.x > self.maxLoadedRegion.x:
            print("Pruning world width around: " + str(x))
            self.PruneByX(x)

        if extents.y > self.maxLoadedRegion.y:
            print("Pruning world height around: " + str(y))
            self.PruneByY(y)

    def PruneByX(self, x):
        minX = x - self.maxLoadedRegion.x / 2.0
        maxX = x + self.maxLoadedRegion.x / 2.0
        toRemove = []
        with self.chunksLock:
            for (coords, tiles) in self.chunks.items():
                if coords.x < minX:
                    toRemove.append(coords)
                elif coords.x + len(tiles) > maxX:
                    toRemove.append(coords)

        for coords in toRemove:
            self.RemoveTilesAt(coords)

        self.minLoadedCoords.x = minX
        self.maxLoadedCoords.x = maxX

    def PruneByY(self, y):
        minY = y - self.maxLoadedRegion.y / 2.0
        maxY = y + self.maxLoadedRegion.y / 2.0
        toRemove = []
        
        with self.chunksLock:
            for (coords, tiles) in self.chunks.items():
                if coords.y < minY:
                    toRemove.append(coords)
                elif coords.y + len(tiles[0]) > maxY:
                    toRemove.append(coords)

        for coords in toRemove:
            self.RemoveTilesAt(coords)

        self.minLoadedCoords.y = minY
        self.maxLoadedCoords.y = maxY
