from datetime import datetime
import os
from pubsub import pub
import time
import pickle

class ChunkWriter:
    def __init__(self, path = ".", chunkNamePrefix="chunk", makeUniqueID = False, writePickled = True, writeHumanReadable = False):
        self.pickledPath = path
        self.humanReadablePath = path + "-readable"
        self.chunkNamePrefix = chunkNamePrefix
        self.writePickled = writePickled
        self.writeHumanReadable = writeHumanReadable
        if makeUniqueID:
            timestampID = datetime.now().strftime('%Y-%m-%d-%H-%M-%S-%f')
            uniqueID = 0

            while os.path.exists(self.pickledPath + timestampID + str(uniqueID)) or \
                    os.path.exists(self.humanReadablePath + timestampID + str(uniqueID)):
                uniqueID += 1

            self.pickledPath = self.pickledPath + timestampID + str(uniqueID)
            self.humanReadablePath = self.humanReadablePath + timestampID + str(uniqueID)

        if self.writePickled:
            os.makedirs(self.pickledPath, exist_ok=True)
        if self.writeHumanReadable:
            os.makedirs(self.humanReadablePath, exist_ok=True)

        pub.subscribe(self.WriteChunk, "tilegen.created")

    def WriteChunk(self, chunkPos, tiles):
        startTime = time.time()
        chunkFileName = self.chunkNamePrefix + "_" + str(chunkPos.x) + "_" + str(chunkPos.y)
        if self.writePickled:
            filePath = os.path.join(self.pickledPath, chunkFileName)
            with open(filePath, "wb") as chunkFile:
                pickle.dump(tiles, chunkFile)
        if self.writeHumanReadable:
            filePath = os.path.join(self.humanReadablePath, chunkFileName)
            with open(filePath, "w") as chunkFile:
                chunkFile.write(str(tiles))            
        endTime = time.time()
        print("Write time: " + str(endTime-startTime))
