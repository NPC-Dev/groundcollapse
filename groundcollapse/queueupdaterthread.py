from groundcollapse import buildqueue
from groundcollapse import levelbuilder
from groundcollapse import levelstore
from groundcollapse import tileset
import threading
import pygame

class QueueUpdaterThread(threading.Thread):

    def __init__(self, cameraRect, levelStore, chunkSize, worldBuildScreens):
        threading.Thread.__init__(self)
        self.cameraRect = cameraRect
        self.levelStore = levelStore
        self.chunkSize = chunkSize
        self.worldBuildScreens = worldBuildScreens
        self.chunkQueue = None

    def run(self):
        self.chunkQueue = self.UpdateChunkQueue()

    def UpdateChunkQueue(self):
        camRect = pygame.Rect(self.cameraRect)
        levelStore = self.levelStore
        chunkSize = self.chunkSize
        worldBuildScreens = self.worldBuildScreens

        chunkQueue = buildqueue.BuildQueue(self, self.levelStore, self.chunkSize)

        camRect.inflate_ip((worldBuildScreens-1) * camRect.width, (worldBuildScreens-1) * camRect.height)

        # Align camera to a chunk
        camRect.x = int(camRect.x / chunkSize.x)*chunkSize.x
        camRect.y = int(camRect.y / chunkSize.y)*chunkSize.y

        for x in range(camRect.x, camRect.x + camRect.width, int(chunkSize.x)):
            for y in range(camRect.y, camRect.y + camRect.height, int(chunkSize.y)):
                if levelStore.Contains(x, y):
                    continue

                chunkQueue.AddItem(tileset.Pos(x,y))

        chunkQueue.OnUpdatesFinished()
        return chunkQueue

    def GetCameraRect(self):
        result = self.cameraRect
        return result

    def GetCameraCenter(self):
        result = pygame.Vector2(self.cameraRect.x + self.cameraRect.width/2.0, self.cameraRect.y + self.cameraRect.height/2.0)
        return result

    def GetQueue(self):
        return self.chunkQueue


