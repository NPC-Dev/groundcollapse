import pygame

class CameraHolder():
    def __init__(self, source=None):
        self.SetCameraSource(source)

    def SetCameraSource(self, source):
        self.cameraSource = source

    def GetCameraCenter(self):
        if self.cameraSource == None:
            return pygame.Vector2(0,0)
        else:
            return self.cameraSource.GetCameraCenter()

    def GetCameraRect(self):
        if self.cameraSource == None:
            return pygame.Vector2(0,0)
        else:
            return self.cameraSource.GetCameraRect()


