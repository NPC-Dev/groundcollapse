#! /usr/bin/python

import math
import pygame
from pubsub import pub

black = (0,0,0)

class LevelViewer:

    def __init__(self, 
                    store,
                    chunkSize,
                    display_width=1280, 
                    display_height=920, 
                    scale_x=2, 
                    scale_y=2,
                    maxCameraVel=500.0,
                    cameraAccel=pygame.Vector2(1000,1000),
                    cameraDecelMod = 5.0,
                    initPygame = True):
        self.loadedImages = {}
        self.levelData = store
        self.chunkSize = chunkSize
        self.clock = pygame.time.Clock()
        
        if initPygame:
            pygame.init()
            self.display = pygame.display.set_mode((display_width, display_height))
        else:
            self.display = None

        self.unscaledDisplay = pygame.Surface((int(display_width/scale_x), int(display_height/scale_y)))
        self.unscaledDisplayRect = self.unscaledDisplay.get_rect()
        
        self.cameraVel = pygame.Vector2()
        self.desiredVel = pygame.Vector2()

        # TODO: load these from a config file?
        self.maxCameraVel = maxCameraVel
        self.cameraAccel = cameraAccel
        self.cameraDecelMod = cameraDecelMod

        self.cameraPos = pygame.Vector2()
        
        self.lastTick = 0

    def SetTilesetData(self, tileset):
        self.tileset = tileset

    def SetLevelStore(self, store):
        self.levelData = store

    def ProcessInput(self, timeDiff):
        for event in pygame.event.get():
            if self.display:
                # If we are managing pygame, also manage quitting.
                if event.type == pygame.QUIT:
                    pygame.quit()
                    return False

            if event.type == pygame.KEYUP:
                if event.key == pygame.K_ESCAPE:
                    pub.sendMessage("mode.changed", mode="mainmenu")
                    return True
       
        self.ProcessHeldKeys(timeDiff)
        return True

    def ProcessHeldKeys(self, timeDiff):
        heldKeys = pygame.key.get_pressed()
        self.desiredVel = self.cameraVel * self.cameraDecelMod * -1.0

        if heldKeys[pygame.K_a]:
            self.desiredVel.x -= self.cameraAccel.x
        
        if heldKeys[pygame.K_d]:
            self.desiredVel.x += self.cameraAccel.x

        if heldKeys[pygame.K_w]:
            self.desiredVel.y -= self.cameraAccel.y

        if heldKeys[pygame.K_s]:
            self.desiredVel.y += self.cameraAccel.y
    
    def Tick(self, tickDiff = None, display = None):
        if not display:
            display = self.display

        if not tickDiff:
            # If we were not provided a tick time difference, calculate it.
            newTime = pygame.time.get_ticks()
            tickDiff = (newTime - self.lastTick)/1000.0
            self.lastTick = newTime
        
        if not self.ProcessInput(tickDiff):
            return False

        self.ProcessCameraMovement(tickDiff)

        self.Clear()

        self.DrawLevel()

        display.blit(pygame.transform.scale(self.unscaledDisplay, display.get_rect().size), (0, 0))

        if self.display:
            # If we are managing our own display, update pygame main display and tick
            pygame.display.update() 
            self.clock.tick(60)

        return True

    def Clear(self):
        if self.display:
            self.display.fill((black))
        self.unscaledDisplay.fill((black))

    def DrawLevel(self):
        minChunk = self.ToTileCoords(self.cameraPos)
        minChunk.x /= self.chunkSize.x
        minChunk.y /= self.chunkSize.y
        maxChunk = self.ToTileCoords(self.cameraPos + self.unscaledDisplayRect.size)
        maxChunk.x /= self.chunkSize.x
        maxChunk.y /= self.chunkSize.y
        maxChunk.x += 1
        maxChunk.y += 1
        
        for chunkX in range(math.floor(minChunk.x), math.floor(maxChunk.x)):
            for chunkY in range(math.floor(minChunk.y), math.floor(maxChunk.y)):
                tiles = self.levelData.GetTiles(chunkX * self.chunkSize.x, chunkY * self.chunkSize.y)

                self.DrawChunk(pygame.Vector2(chunkX, chunkY), tiles)

    def DrawChunk(self, chunkPos, tiles):
        tileSize = self.tileset.GetTileSize()
        
        for x in range(0, len(tiles)):
            for y in range(0, len(tiles[0])):
                tileScreenX = (chunkPos.x * self.chunkSize.x + x) * tileSize.x - self.cameraPos.x
                tileScreenY = (chunkPos.y * self.chunkSize.y + y) * tileSize.y - self.cameraPos.y
                tileRect = pygame.Rect(tileScreenX, tileScreenY, tileSize.x, tileSize.y)
                if not tileRect.colliderect(self.unscaledDisplayRect):
                    continue
                self.DrawTile(tiles[x][y], tileScreenX, tileScreenY)

    def ToTileCoords(self, pos):
        tilesize = self.tileset.GetTileSize()
        return pygame.Vector2(pos.x / tilesize.x, pos.y / tilesize.y)

    def DrawTile(self, tile, x, y):
        tileMetadata = self.tileset.GetTile(tile)
        if tileMetadata == None:
            return

        pos = tileMetadata.GetPos()
        size = self.tileset.GetTileSize()

        tileImage = self.LoadOrGetImage(tileMetadata.GetFile())

        self.unscaledDisplay.blit(tileImage, (x,y), (pos.x, pos.y, size.x, size.y))

    def ProcessCameraMovement(self, timeDiff):
        self.cameraVel = self.cameraVel.lerp(self.desiredVel, min(timeDiff,1.0))
        if self.cameraVel.magnitude_squared() > (self.maxCameraVel * self.maxCameraVel):
            self.cameraVel.scale_to_length(self.maxCameraVel)

        self.cameraPos += self.cameraVel * timeDiff

    def SetCamera(self, x, y):
        self.cameraPos.x = x
        self.cameraPos.y = y
    
    def SetCamera(self, vector):
        self.cameraPos = vector

    def GetCameraRect(self):
        result = pygame.Rect(self.ToTileCoords(self.cameraPos), 
                            self.ToTileCoords(pygame.Vector2(self.unscaledDisplayRect.size)))
        return result

    def GetCameraCenter(self):
        result = self.ToTileCoords(pygame.Vector2(self.cameraPos.x + self.unscaledDisplayRect.width/2, 
                            self.cameraPos.y + self.unscaledDisplayRect.height/2))
        return result

    def LoadOrGetImage(self, filename):
        image = self.loadedImages.get(filename)
        if image:
            return image

        image = pygame.image.load(filename)
        image = image.convert()

        self.loadedImages[filename] = image
        return image

if __name__ == "__main__":
    viewer = LevelViewer()
    while(viewer.Tick()):
        continue

    quit()
