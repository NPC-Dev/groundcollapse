from groundcollapse import buildqueue
import threading
import pygame

class QueueSorterThread(threading.Thread):

    def __init__(self, cameraRect, chunkQueue):
        threading.Thread.__init__(self)
        self.cameraRect = cameraRect
        self.chunkQueue = chunkQueue

    def run(self):
        self.UpdateChunkQueue()

    def UpdateChunkQueue(self):
        self.chunkQueue.SetCameraHolder(self)
        self.chunkQueue.OnUpdatesFinished()

    def GetCameraRect(self):
        result = self.cameraRect
        return result

    def GetCameraCenter(self):
        result = pygame.Vector2(self.cameraRect.x + self.cameraRect.width/2.0, self.cameraRect.y + self.cameraRect.height/2.0)
        return result

    def GetQueue(self):
        return self.chunkQueue


