import pygame
from pygame_gui.elements.ui_panel import UIPanel
from pygame_gui.elements.ui_window import UIWindow
from pygame_gui.elements.ui_label import UILabel
from pygame_gui.elements.ui_image import UIImage
from pygame_gui.elements.ui_scrolling_container import UIScrollingContainer
from pygame_gui.elements.ui_text_entry_line import UITextEntryLine
from groundcollapse.editor import tileadjacencypanel
from groundcollapse.editor import tilewalkabilitypanel
from groundcollapse import tileset

WIDTH_PADDING = 25
HEIGHT_PADDING = 20
TITLEBAR_HEIGHT = 50

class TileSettingsWindow(UIWindow):
    def __init__(self, 
                rect, 
                manager, 
                imageLoader,
                element_id="tilesettings",
                tile=None,
                tileset=None
                ):
        UIWindow.__init__(self, rect=rect, manager=manager, element_id=element_id, window_display_title="Tile Settings")

        self.imageLoader = imageLoader

        self.scrollingContainer = UIScrollingContainer(
                                        relative_rect=pygame.Rect(
                                            int(WIDTH_PADDING),
                                            int(HEIGHT_PADDING),
                                            int(self.relative_rect.width - 3*WIDTH_PADDING),
                                            int(self.relative_rect.height - TITLEBAR_HEIGHT - HEIGHT_PADDING * 2)
                                        ), 
                                        manager=self.ui_manager, 
                                        container=self, 
                                        parent_element=self, 
                                        object_id="scrollingcontainer"
                                    )
       
        self.tileAdjacency = tileadjacencypanel.TileAdjacencyPanel(
                                                    rect=pygame.Rect(
                                                        0,
                                                        0,
                                                        self.scrollingContainer.relative_rect.width - 2*WIDTH_PADDING,
                                                        self.scrollingContainer.relative_rect.height - 2*HEIGHT_PADDING
                                                    ), 
                                                    manager=self.ui_manager, 
                                                    imageLoader=self.imageLoader, 
                                                    container=self.scrollingContainer
                                                )
        self.tileAdjacency.SetTile(tile, tileset)

        self.tileWalkabilityPanel = tilewalkabilitypanel.TileWalkabilityPanel(
                                                    rect=pygame.Rect(
                                                        0,
                                                        self.tileAdjacency.relative_rect.bottom,
                                                        self.scrollingContainer.relative_rect.width - 2*WIDTH_PADDING,
                                                        self.scrollingContainer.relative_rect.height - 2*HEIGHT_PADDING
                                                    ),
                                                    manager=self.ui_manager,
                                                    imageLoader=self.imageLoader,
                                                    container=self.scrollingContainer
                                                )
        self.tileWalkabilityPanel.SetTile(tile, tileset)
        self.tileWalkabilityPanel.set_dimensions((self.tileWalkabilityPanel.relative_rect.width, self.tileWalkabilityPanel.GetDesiredHeight()))

        self.UpdateScrollingContainerDimensions()

    def UpdateScrollingContainerDimensions(self):
        self.scrollingContainer.set_scrollable_area_dimensions(
            (self.scrollingContainer.scrolling_width,
            self.tileWalkabilityPanel.relative_rect.bottom + HEIGHT_PADDING))
        self.scrollingContainer.set_scrollable_area_dimensions(
            (self.scrollingContainer.scrolling_width-self.scrollingContainer.scroll_bar_width, 
            self.scrollingContainer.scrolling_height))