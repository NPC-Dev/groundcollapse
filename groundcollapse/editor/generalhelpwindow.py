from pygame_gui.windows.ui_message_window import UIMessageWindow

GENERAL_HELP_MESSAGE = \
"""You can add individual tiles and associate them with specific sprites (or areas from a sprite sheet) here.<br> <br>You can also slice sprites in an area of a sprite sheet using the "Add Sprite Sheet" option from the Edit menu.<br> <br>Next, you should set up which tiles are allowed to be next to each other. To do so, use the settings from the "Edit Rules" button on each tile.<br> <br>You can also set up additional settings, such as whether each edge and corner of the tile can be walked through, in that same settings window.<br> <br>Remember to save your changes frequently.<br> <br>You can test your current tileset from the "File->Generate" menu option (even if it's not saved - but remember to save it before you quit!) - press Escape when testing to return to the main menu.<br> <br>You can undo and redo your changes - and if you change windows or selected tiles, you can still undo and redo older changes you made (so you don't lose history - but be careful undoing changes you can't currently see).<br> <br>If you set an invalid value in a text box, you will see the text turn red. That means the change has not actually been made, because it would be invalid.<br> <br>If you have made changes to a text box that are not yet set, it will turn yellow. Press enter to confirm the change.
"""

class GeneralHelpWindow(UIMessageWindow):
    
    def __init__(self, rect, manager, object_id="general_help"):
        UIMessageWindow.__init__(self,
            rect = rect,
            manager=manager,
            window_title="How To Use",
            object_id=object_id,
            html_message=GENERAL_HELP_MESSAGE
        )