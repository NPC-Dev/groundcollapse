import pygame
import pygame_gui
from pubsub import pub
from groundcollapse.editor import mainmenu
from groundcollapse.editor import tileseteditor
from groundcollapse import levelviewer
from groundcollapse import levelstore
from groundcollapse import infinitegroundgenerator
from groundcollapse import cameraholder
from groundcollapse import levelbuilder
from groundcollapse import tileset

black = (0,0,0)
ALPHA_KEY_COLOR=(255,255,0)

FPS = 60

class Editor:
        
    def __init__(self,
                    tilesetLoader,
                    display_width=1280,
                    display_height=920,
                    scale_x = 1,
                    scale_y = 1,
                    tileset = None,
                    tileExtraDataFactory = None,
                    args = None,
                    cameraHolder = None):
        pygame.init()

        self.args = args

        self.cameraHolder = cameraholder.CameraHolder()

        self.tilesetLoader = tilesetLoader
        self.tileExtraDataFactory = tileExtraDataFactory
        
        self.lastTick = 0
        self.clock = pygame.time.Clock()
        
        self.mainMenu = None
        self.display = pygame.display.set_mode((display_width, display_height))

        self.UnsetViewer()

        self.displayedScreen = None

        self.guiManager = pygame_gui.UIManager((display_width, display_height))
        self.LoadFonts()
        
        self.tilesetEditor = None

        self.quitting = False

        self.tileset = tileset

        self.SubscribeEvents()

        self.ShowMainMenu()

    def UnsetViewer(self):
        self.levelViewer = None
        self.groundGenerator = None
        self.builder = None
        self.levelStore = None
        self.cameraStore = None

    def SubscribeEvents(self):
        pub.subscribe(self.ModeChanged, "mode.changed")
        pub.subscribe(self.DoQuit, "quit")
        pub.subscribe(self.OnTilesetLoaded, "tileset.loaded")

    def OnTilesetLoaded(self, tiles):
        if self.mainMenu:
            self.mainMenu.SetGeneratorEnabled(True)
        self.tileset = tiles

    def DoQuit(self):
        pygame.quit()
        self.quitting = True        

    def ModeChanged(self, mode):
        if mode == "levelviewer":
            self.ShowLevelViewer()
        elif mode == "tileseteditor":
            self.ShowTilesetEditor()
        elif mode == "mainmenu":
            self.ShowMainMenu()
        else:
            print("Unknown mode: " + mode)

    def ShowLevelViewer(self):
        args = self.args
        failureSeconds = args.failureMillis/1000.0
        fallbackOptimizeSeconds = args.fallbackOptimizeMillis/1000.0
        optimizationSeconds = args.optimizationMillis/1000.0

        tiles = self.tileset 
        
        self.builder = levelbuilder.LevelBuilder(tiles, 
                                        args.path, 
                                        args.program, 
                                        seed = int(args.seed), 
                                        levelSize = int(args.size), 
                                        randFreq=args.rand_freq, 
                                        numCores = args.cores, 
                                        aggregateStats=args.aggregateStats)
    
        chunkSize = pygame.Vector2(args.size, args.size)
        
        maxLoadedRegion = self.DetermineMaxLoadedRegion(self.builder, args)
        self.levelStore = levelstore.LevelStore(maxLoadedRegion, self.cameraHolder)
        
        self.levelViewer = levelviewer.LevelViewer(self.levelStore, 
                                            chunkSize, 
                                            args.display_width, 
                                            args.display_height, 
                                            args.scale_width, 
                                            args.scale_height, 
                                            initPygame=False)
        self.levelViewer.SetTilesetData(tiles)

        self.cameraHolder.SetCameraSource(self.levelViewer)

        self.groundGenerator = infinitegroundgenerator.InfiniteGroundGenerator( 
                                        self.cameraHolder, 
                                        self.levelStore, 
                                        self.builder, 
                                        args.debugOnFallback, 
                                        chunkSize,
                                        args.worldBuildScreens, 
                                        fallbackOptimizeSeconds, 
                                        optimizationSeconds,
                                        failureSeconds,
                                        args.optimizationScreens)

        self.displayedScreen = self.levelViewer
        
    def Finalize(self):
        if self.builder:
            self.builder.Interrupt()
            self.builder.PrintResult()
            if self.args.aggregateStats:
                self.builder.PrintAvgStats()

    def DetermineMaxLoadedRegion(self, builder, args):
        tileSize = builder.GetTileset().GetTileSize()
        screenSizeTilePixels = tileset.Pos(args.display_width/args.scale_width, args.display_height/args.scale_height)
        screenSizeTiles = tileset.Pos(screenSizeTilePixels.x/tileSize.x, screenSizeTilePixels.y/tileSize.y)

        maxLoadedRegion = pygame.Vector2(screenSizeTiles.x*args.maxLoadedScreens, screenSizeTiles.y*args.maxLoadedScreens)
        return maxLoadedRegion

    def LoadFonts(self):
        self.guiManager.add_font_paths(font_name='Megrim-Regular', regular_path='fonts/Megrim-Regular.ttf')
        self.guiManager.preload_fonts([{'name': 'Megrim-Regular', 'point_size': 48, 'style': 'regular'},
                                       {'name': 'Megrim-Regular', 'point_size': 24, 'style': 'regular'},
                                       {'name': 'Megrim-Regular', 'point_size': 14, 'style': 'regular'},
                                       ])
    def ShowMainMenu(self):
        if self.mainMenu == None:
            self.mainMenu = mainmenu.MainMenu(self.display.get_rect(), self.guiManager)
        
        self.mainMenu.SetGeneratorEnabled(self.tileset != None)

        self.Finalize()
        self.UnsetViewer()

        self.displayedScreen = self.mainMenu

    def ShowTilesetEditor(self):
        if self.tilesetEditor == None:
            self.tilesetEditor = tileseteditor.TilesetEditor(self.display.get_rect(), 
                                                                self.guiManager, 
                                                                tilesetLoader = self.tilesetLoader, 
                                                                tileset = self.tileset,
                                                                tileExtraDataFactory = self.tileExtraDataFactory)

        self.Finalize()
        self.UnsetViewer()

        self.displayedScreen = self.tilesetEditor

    def ProcessInput(self, timeDiff):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.DoQuit()
                return False

            if self.ProcessGUI(event):
                continue

        return True

    def ProcessGUI(self, event):
        return self.guiManager.process_events(event)

    def Tick(self):
        if self.quitting:
            return False

        newTime = pygame.time.get_ticks()
        tickDiff = (newTime - self.lastTick)/1000.0
        self.lastTick = newTime

        self.Clear()
        
        if not self.ProcessInput(tickDiff):
            return False

        if self.quitting:
            return False
        
        self.guiManager.update(tickDiff)

        if self.displayedScreen == self.levelViewer:
            self.groundGenerator.Tick(tickDiff=tickDiff)

        if self.displayedScreen:
            self.displayedScreen.Tick(tickDiff=tickDiff, display=self.display)

        # TODO: Update any other displays here.

        self.guiManager.draw_ui(self.display)
        
        pygame.display.update()
        
        self.clock.tick(FPS)

        return True

    def Clear(self):
        self.display.fill((black))


if __name__ == "__main__":
    editor = Editor()
    while(editor.Tick()):
        continue

