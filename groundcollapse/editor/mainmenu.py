from pygame_gui.elements.ui_panel import UIPanel
from pygame_gui.elements.ui_image import UIImage
from pygame_gui.elements.ui_button import UIButton
import pygame
from pubsub import pub

LOGO_WIDTH = 500

BUTTON_PADDING_HEIGHT = 20

class MainMenu(UIPanel):
    def __init__(self, rect, manager, element_id="mainmenu", gameInProgress = False):
        UIPanel.__init__(self, relative_rect=rect, starting_layer_height=1, manager=manager, element_id=element_id)
    
        self.SubscribeEvents()

        self.logoImageSprite = pygame.image.load('sprites/npcdev/GroundCollapseLogoTransparent.png').convert_alpha()

        logoScale = (LOGO_WIDTH/self.logoImageSprite.get_rect().width)
       
        self.logoImageSprite = pygame.transform.scale(self.logoImageSprite, (LOGO_WIDTH, int(self.logoImageSprite.get_rect().height * logoScale)))
        self.logoImage = UIImage(pygame.Rect((int(self.rect.width/2 - LOGO_WIDTH/2),
                                               int(20)),
                                              self.logoImageSprite.get_rect().size),
                                  self.logoImageSprite, self.ui_manager,
                                  container=self.get_container(),
                                  parent_element=self)

        self.levelViewerButton = UIButton(relative_rect=pygame.Rect((rect.width/2-100, self.logoImage.relative_rect.bottom + BUTTON_PADDING_HEIGHT),
                                                                      (200, 60)),
                                            text='Generate',
                                            manager=self.ui_manager,
                                            container=self.get_container(),
                                            parent_element=self
                                        )
        
        self.tilesetEditorButton = UIButton(relative_rect=pygame.Rect((rect.width/2-100, self.levelViewerButton.relative_rect.y + self.levelViewerButton.relative_rect.height + BUTTON_PADDING_HEIGHT),
                                                                      (200, 60)),
                                            text='Edit Tileset',
                                            manager=self.ui_manager,
                                            container=self.get_container(),
                                            parent_element=self
                                        )
        
        self.quitButton = UIButton(relative_rect=pygame.Rect((rect.width/2-100, self.tilesetEditorButton.relative_rect.y + self.tilesetEditorButton.relative_rect.height + BUTTON_PADDING_HEIGHT),
                                                                      (200, 60)),
                                            text='Quit',
                                            manager=self.ui_manager,
                                            container=self.get_container(),
                                            parent_element=self
                                        )

    def SubscribeEvents(self):
        pub.subscribe(self.ModeChanged, "mode.changed")
        pub.subscribe(self.DoQuit, "quit")

    def ModeChanged(self, mode):
        if mode == "mainmenu":
            self.show()
        else:
            self.hide()

    def SetGeneratorEnabled(self, enabled):
        if enabled:
            self.levelViewerButton.enable()
        else:
            self.levelViewerButton.disable()

    def process_event(self, event):
        processed_event = False
        
        if event.type == pygame.USEREVENT:
            if event.user_type == 'ui_button_pressed':
                if event.ui_element == self.levelViewerButton:
                    pub.sendMessage("mode.changed", mode="levelviewer")
                    processed_event = True

                if event.ui_element == self.tilesetEditorButton:
                    pub.sendMessage("mode.changed", mode="tileseteditor")
                    processed_event = True
                
                if event.ui_element == self.quitButton:
                    self.kill()
                    pub.sendMessage("quit")
                    processed_event = True
        
        return processed_event
    
    def DoQuit(self):
        pygame.quit()

    def Tick(self, tickDiff, display):
        pass
