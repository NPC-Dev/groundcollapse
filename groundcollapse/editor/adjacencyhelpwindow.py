from pygame_gui.windows.ui_message_window import UIMessageWindow

ADJACENCY_HELP_MESSAGE = \
"""Set which tiles are allowed to be adjacent to this tile.<br> <br>The tile in the middle of the displayed cross-section of tiles is the current tile you are editing. You can change allowed adjacencies on one side at a time (shown by the blue edit button in that direction). The other tiles shown are previews of how other tiles would look if placed on that side of the tile.<br> <br>By default, a tile is not allowed to be next to any other tiles (so if both buttons are grey, the tile is not permitted to be next to this one on the chosen side). You need to set up all valid neighbors here.<br> <br>First, select a direction to edit by clicking the "Edit" button on the appropriate edge.<br> <br>You can preview what a tile will look like next to this tile by hovering over it.<br> <br>You can set adjacencies in three ways:<br> <br>1. Allow a specific tile, by clicking on the name of the tile. It will turn blue to indicate that it is now allowed explicitly.<br> <br>2. Allow a tile by tag. You can type tags in the "Edge Tags" box, and if the opposite edge on the other tile has the same tag, they will be allowed to be adjacent to each other. This will be shown by a locked selected state and a (tag) in parentheses, to show which tag is allowing the adjacency.<br> <br>3. Finally, you can specifically PREVENT adjacency (even if tags would normally allow it) by clicking the "Prohibit" button below the tile in the list. This will prohibit that tile from being next to this one on that side no matter what.
"""

class AdjacencyHelpWindow(UIMessageWindow):
    
    def __init__(self, rect, manager, object_id="adjacency_help"):
        UIMessageWindow.__init__(self,
            rect = rect,
            manager=manager,
            window_title="Setting Up Adjacency",
            object_id=object_id,
            html_message=ADJACENCY_HELP_MESSAGE
        )