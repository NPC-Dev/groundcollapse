import pygame
import pygame_gui
from pubsub import pub
from pygame_gui.elements.ui_window import UIWindow
from groundcollapse import tileset
from pygame_gui.elements.ui_label import UILabel
from pygame_gui.elements.ui_button import UIButton
from pygame_gui.elements.ui_image import UIImage
from pygame_gui.elements.ui_scrolling_container import UIScrollingContainer
from pygame_gui.elements.ui_text_entry_line import UITextEntryLine
from pygame_gui.elements.ui_drop_down_menu import UIDropDownMenu
from pygame_gui.windows.ui_file_dialog import UIFileDialog

WIDTH_PADDING=25
HEIGHT_PADDING=20
TEXT_HEIGHT=25
EDIT_BUTTON_WIDTH = 100
SLICE_BUTTON_WIDTH = 200

ERROR_COLOR = (255,0,0)
CHANGED_TEXT_COLOR = (255, 255, 180)
SLICE_LINE_COLOR = (255,0,255)
SLICE_LINE_WIDTH = 1

class SpriteSheetImporter(UIWindow):
    def __init__(self, 
                rect, 
                manager, 
                imageLoader,
                editor,
                element_id="spritesheetimporter",
                tileset=None,
                ):
        UIWindow.__init__(self, rect=rect, manager=manager, element_id=element_id, window_display_title="Tile Settings")

        self.editor = editor
        
        self.imageLoader = imageLoader
    
        self.tileset = tileset

        self.imagePathLabel = UILabel(
                                    relative_rect=pygame.Rect(
                                        int(WIDTH_PADDING), 
                                        int(HEIGHT_PADDING), 
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)), 
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Tileset File", 
                                    manager=manager, 
                                    container=self
                                )

        self.imagePathButton = UIButton(
                                    relative_rect=pygame.Rect(
                                        int(self.imagePathLabel.relative_rect.right + WIDTH_PADDING),
                                        int(self.imagePathLabel.relative_rect.top),
                                        int(EDIT_BUTTON_WIDTH),
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Pick",
                                    manager=manager, 
                                    container=self, 
                                    parent_element=self, 
                                    object_id="imagepathbutton"
                                )

        self.imagePathBox = UITextEntryLine(
                                    relative_rect=pygame.Rect(
                                        int(self.imagePathButton.relative_rect.right + WIDTH_PADDING),
                                        int(self.imagePathButton.relative_rect.top),
                                        int(self.relative_rect.width - self.imagePathButton.relative_rect.right - (3 * WIDTH_PADDING)),
                                        int(TEXT_HEIGHT)
                                    ), 
                                    manager=manager, 
                                    container=self, 
                                    parent_element=self, 
                                    object_id="imagepath"
                                )
        self.defaultTextColor = self.imagePathBox.text_colour

        self.imagePathBox.set_text("File Path")

        self.tileXLabel = UILabel(
                                    relative_rect=pygame.Rect(
                                        int(WIDTH_PADDING), 
                                        int(self.imagePathBox.relative_rect.bottom + HEIGHT_PADDING), 
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)), 
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="X Offset", 
                                    manager=manager, 
                                    container=self
                                )
                                
        self.offsetXTextBox = UITextEntryLine(
                                    relative_rect=pygame.Rect(
                                        int(self.tileXLabel.relative_rect.right + WIDTH_PADDING),
                                        int(self.tileXLabel.relative_rect.top),
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)),
                                        int(TEXT_HEIGHT)
                                    ), 
                                    manager=manager, 
                                    container=self,
                                    parent_element=self, 
                                    object_id="tilexbox"
                                )

        self.tileYLabel = UILabel(
                                    relative_rect=pygame.Rect(
                                        int(self.offsetXTextBox.relative_rect.right + WIDTH_PADDING), 
                                        int(self.imagePathBox.relative_rect.bottom + HEIGHT_PADDING), 
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)), 
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Y Offset", 
                                    manager=manager, 
                                    container=self
                                )
                                
        self.offsetYTextBox = UITextEntryLine(
                                    relative_rect=pygame.Rect(
                                        int(self.tileYLabel.relative_rect.right + WIDTH_PADDING),
                                        int(self.tileYLabel.relative_rect.top),
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)),
                                        int(TEXT_HEIGHT)
                                    ), 
                                    manager=manager, 
                                    container=self, 
                                    parent_element=self, 
                                    object_id="tileybox"
                                )
        
        self.maxXLabel = UILabel(
                                    relative_rect=pygame.Rect(
                                        int(WIDTH_PADDING), 
                                        int(self.offsetYTextBox.relative_rect.bottom + HEIGHT_PADDING), 
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)), 
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Cutoff X", 
                                    manager=manager, 
                                    container=self
                                )
                                
        self.maxXTextBox = UITextEntryLine(
                                    relative_rect=pygame.Rect(
                                        int(self.maxXLabel.relative_rect.right + WIDTH_PADDING),
                                        int(self.maxXLabel.relative_rect.top),
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)),
                                        int(TEXT_HEIGHT)
                                    ), 
                                    manager=manager, 
                                    container=self,
                                    parent_element=self, 
                                    object_id="maxxbox"
                                )

        self.maxYLabel = UILabel(
                                    relative_rect=pygame.Rect(
                                        int(self.maxXTextBox.relative_rect.right + WIDTH_PADDING), 
                                        int(self.offsetXTextBox.relative_rect.bottom + HEIGHT_PADDING), 
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)), 
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Cutoff Y", 
                                    manager=manager, 
                                    container=self
                                )
                                
        self.maxYTextBox = UITextEntryLine(
                                    relative_rect=pygame.Rect(
                                        int(self.maxYLabel.relative_rect.right + WIDTH_PADDING),
                                        int(self.maxYLabel.relative_rect.top),
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)),
                                        int(TEXT_HEIGHT)
                                    ), 
                                    manager=manager, 
                                    container=self, 
                                    parent_element=self, 
                                    object_id="maxybox"
                                )

        self.previewLabel = UILabel(
                                    relative_rect=pygame.Rect(
                                        int(WIDTH_PADDING), 
                                        int(self.maxYLabel.relative_rect.bottom + HEIGHT_PADDING), 
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)), 
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Preview", 
                                    manager=manager, 
                                    container=self
                                )
        
        self.sliceButton = UIButton(
                                    relative_rect=pygame.Rect(
                                        int(self.previewLabel.relative_rect.right + WIDTH_PADDING),
                                        int(self.previewLabel.relative_rect.top),
                                        int(SLICE_BUTTON_WIDTH),
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Add These Tiles!",
                                    manager=manager, 
                                    container=self, 
                                    parent_element=self, 
                                    object_id="slicebutton"
                                )

                                
        self.previewImage = None
        self.baseImage = None

        self.tileSize = tileset.GetTileSize()
        self.tileOffset = pygame.Vector2(0,0)

        self.tileMax = None

        self.filePath = ""

    def process_event(self, event):
        handled = super().process_event(event)
        if event.type != pygame.USEREVENT:
            return False

        if event.type == pygame.USEREVENT:
            if event.user_type == pygame_gui.UI_TEXT_ENTRY_CHANGED:
                self.HandleTextChange(event)

            elif event.user_type == pygame_gui.UI_TEXT_ENTRY_FINISHED:
                self.HandleTextEntry(event)

            elif event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                if event.ui_element == self.imagePathButton:
                    self.editor.CreateFilePicker(self.GetFilePath(), self.ChangeFilePath);
                elif event.ui_element == self.sliceButton:
                    if self.FinalizeImport():
                        self.kill()
    
    def GetFilePath(self):
        return self.filePath

    def HandleTextChange(self, event):
        if event.ui_element == self.imagePathBox or \
            event.ui_element == self.offsetXTextBox or \
            event.ui_element == self.offsetYTextBox or \
            event.ui_element == self.maxXTextBox or \
            event.ui_element == self.maxYTextBox:
            event.ui_element.text_colour = CHANGED_TEXT_COLOR

    def HandleTextEntry(self, event):
        if event.ui_element == self.imagePathBox or \
            event.ui_element == self.offsetXTextBox or \
            event.ui_element == self.offsetYTextBox or \
            event.ui_element == self.maxXTextBox or \
            event.ui_element == self.maxYTextBox:
            event.ui_element.text_colour = self.defaultTextColor
        
        if event.ui_element == self.imagePathBox:
            self.ChangeFilePath(event.text)
        elif event.ui_element == self.offsetXTextBox:
            self.ChangeOffsetX(event.text)
        elif event.ui_element == self.offsetYTextBox:
            self.ChangeOffsetY(event.text)
        elif event.ui_element == self.maxXTextBox:
            self.ChangeMaxX(event.text)
        elif event.ui_element == self.maxYTextBox:
            self.ChangeMaxY(event.text)

    def ChangeOffsetX(self, x):
        try:
            self.tileOffset.x = int(x)
            self.OnOffsetUpdated()
            self.offsetXTextBox.text_colour = self.defaultTextColor
            return True
        except Exception as e:
            self.offsetXTextBox.text_colour = ERROR_COLOR
            return False
    
    def ChangeOffsetY(self, y):
        try:
            self.tileOffset.y = int(y)
            self.OnOffsetUpdated()
            self.offsetYTextBox.text_colour = self.defaultTextColor
            return True
        except Exception as e:
            self.offsetYTextBox.text_colour = ERROR_COLOR
            return False
    
    def ChangeMaxX(self, x):
        try:
            self.tileMax.x = int(x)
            self.OnOffsetUpdated()
            self.maxXTextBox.text_colour = self.defaultTextColor
            return True
        except Exception as e:
            self.maxXTextBox.text_colour = ERROR_COLOR
            return False
    
    def ChangeMaxY(self, y):
        try:
            self.tileMax.y = int(y)
            self.OnOffsetUpdated()
            self.maxYTextBox.text_colour = self.defaultTextColor
            return True
        except Exception as e:
            self.maxYTextBox.text_colour = ERROR_COLOR
            return False

    def ChangeFilePath(self, filePath):
        self.filePath = filePath

        self.imagePathBox.text = self.filePath

        if not self.CreatePreviewImage():
            self.imagePathBox.text_colour = ERROR_COLOR
        else:
            self.imagePathBox.text_colour = self.defaultTextColor

    def CreatePreviewImage(self):
        self.baseImage = self.LoadImage(self.filePath)
        if not self.baseImage:
            return False

        self.tileMax = pygame.Vector2(self.baseImage.get_width(), self.baseImage.get_height())
        self.maxXTextBox.set_text(str(int(self.tileMax.x)))
        self.maxYTextBox.set_text(str(int(self.tileMax.y)))
        self.maxXTextBox.text_colour = self.defaultTextColor
        self.maxYTextBox.text_colour = self.defaultTextColor

        augmentedImage = self.AugmentImage(self.baseImage)

        self.SetImage(augmentedImage)
        return True

    def OnOffsetUpdated(self):
        augmentedImage = self.AugmentImage(self.baseImage)

        self.SetImage(augmentedImage)

    def AugmentImage(self, image):
        newImage = image.copy()

        endCoords = self.DetermineEndCoords(image)
        endX = int(endCoords.x)
        endY = int(endCoords.y)

        for x in range(int(self.tileOffset.x), endX+1, int(self.tileSize.x)):
            startPos = pygame.Vector2(x, self.tileOffset.y)
            endPos = pygame.Vector2(x, endY)
            pygame.draw.line(newImage, SLICE_LINE_COLOR, startPos, endPos, SLICE_LINE_WIDTH)

        for y in range(int(self.tileOffset.y), endY+1, int(self.tileSize.y)):
            startPos = pygame.Vector2(self.tileOffset.x, y)
            endPos = pygame.Vector2(endX, y)
            pygame.draw.line(newImage, SLICE_LINE_COLOR, startPos, endPos, SLICE_LINE_WIDTH)

        return newImage

    def DetermineEndCoords(self, image):
        maxX = image.get_width()
        if self.tileMax != None:
            maxX = int(self.tileMax.x)
        
        maxY = image.get_height()
        if self.tileMax != None:
            maxY = int(self.tileMax.y)

        endX = int(self.tileOffset.x) + int((maxX - self.tileOffset.x)/int(self.tileSize.x)) * int(self.tileSize.x)
        endY = int(self.tileOffset.y) + int((maxY - self.tileOffset.y)/int(self.tileSize.y)) * int(self.tileSize.y)
        return pygame.Vector2(endX, endY)

    def LoadImage(self, filePath):
        loadedImage = self.imageLoader.LoadOrGetImage(filePath)
        return loadedImage

    def SetImage(self, loadedImage):
        if self.previewImage:
            self.previewImage.kill()

        maxWidth = self.relative_rect.width - (2 * WIDTH_PADDING)
        maxHeight = self.relative_rect.height - self.title_bar_height - (self.previewLabel.relative_rect.bottom + 3 * HEIGHT_PADDING)

        scaleRatio = min(maxWidth/loadedImage.get_width(),
                            maxHeight/loadedImage.get_height())

        self.previewImage = UIImage( 
                                    relative_rect=pygame.Rect(
                                        int(WIDTH_PADDING),
                                        int(self.previewLabel.relative_rect.bottom + HEIGHT_PADDING),
                                        int(loadedImage.get_width() * scaleRatio),
                                        int(loadedImage.get_height() * scaleRatio)
                                    ),
                                    image_surface=loadedImage,
                                    manager=self.ui_manager, 
                                    container=self, 
                                    parent_element=self,
                                    object_id="tileimage"
                                )

    def FinalizeImport(self):
        if not self.baseImage:
            return False

        endCoords = self.DetermineEndCoords(self.baseImage)

        for x in range(int(self.tileOffset.x), int(endCoords.x), int(self.tileSize.x)):
            for y in range(int(self.tileOffset.y), int(endCoords.y), int(self.tileSize.y)):
                newTile = self.tileset.CreateEmptyTile()
                newTile.SetFile(self.imagePathBox.text)
                newTile.SetPos(x, y)
                
                pub.sendMessage("tileseteditor.tileadded", tile=newTile)
        
        return True