import pygame
import pygame_gui
from pubsub import pub
from pygame_gui.elements.ui_panel import UIPanel
from pygame_gui.elements.ui_label import UILabel
from pygame_gui.elements.ui_image import UIImage
from pygame_gui.elements.ui_button import UIButton
from pygame_gui.elements.ui_scrolling_container import UIScrollingContainer
from pygame_gui.elements.ui_text_entry_line import UITextEntryLine
from pygame_gui.windows.ui_message_window import UIMessageWindow
from groundcollapse import tileset
from groundcollapse.editor import selectabletilepanel 
from groundcollapse.editor.commands import settextcommand
from groundcollapse.editor.commands import twofunctioncommand
from groundcollapse.editor import adjacencyhelpwindow
import copy

WIDTH_PADDING=25
HEIGHT_PADDING=20
TEXT_HEIGHT=25
TILE_DISPLAY_WIDTH=64
TILE_DISPLAY_HEIGHT=64
TOOLBAR_HEIGHT = 50
QUESTION_BUTTON_WIDTH = 64
QUESTION_BUTTON_HEIGHT = 64

CHANGED_TEXT_COLOR = (255, 255, 180)

DIRS = (tileset.Dir(-1,0), tileset.Dir(1,0), tileset.Dir(0,-1), tileset.Dir(0,1))

class TileAdjacencyPanel(UIPanel):
    def __init__(self, 
                rect, 
                manager, 
                imageLoader,
                element_id="tileadjacency",
                container=None,
                ):
        UIPanel.__init__(self, relative_rect=rect, starting_layer_height=1, anchors={'top': 'top', 'bottom': 'bottom', 'left': 'left', 'right': 'right'}, manager=manager, container=container, element_id=element_id)

        self.imageLoader = imageLoader

        self.tileImage = None

        self.titleLabel = UILabel(
                                    relative_rect=pygame.Rect(
                                                    int(WIDTH_PADDING),
                                                    int(HEIGHT_PADDING),
                                                    int(self.rect.width - 2 * WIDTH_PADDING),
                                                    int(TEXT_HEIGHT)
                                                ),
                                    manager=manager,
                                    container=self,
                                    parent_element=self,
                                    text="Adjacency Settings",
                                    object_id="adjacencytitle"
                                    )

        self.allTilesList = UIScrollingContainer(
                                        relative_rect=pygame.Rect(
                                            int(WIDTH_PADDING),
                                            int(self.titleLabel.relative_rect.bottom + HEIGHT_PADDING),
                                            int(self.rect.width/4),
                                            int(self.rect.height - TOOLBAR_HEIGHT - self.titleLabel.relative_rect.bottom)
                                        ), 
                                        manager=manager, 
                                        container=self, 
                                        parent_element=self, 
                                        object_id="tileslist"
                                    )
        
        self.tagsLabel = UILabel(
                                    relative_rect=pygame.Rect(
                                                    int(self.allTilesList.relative_rect.right),
                                                    int(self.titleLabel.relative_rect.bottom + HEIGHT_PADDING),
                                                    int(self.rect.width/4 - 2 * WIDTH_PADDING),
                                                    int(TEXT_HEIGHT)
                                                ),
                                    manager=manager,
                                    container=self,
                                    parent_element=self,
                                    text="Edge Tags",
                                    object_id="edgetagstitle"
                                )

        self.edgeTagsBox = UITextEntryLine(
                                relative_rect = pygame.Rect(
                                                    int(self.tagsLabel.relative_rect.right + WIDTH_PADDING),
                                                    int(self.tagsLabel.relative_rect.top),
                                                    int(self.relative_rect.width - self.tagsLabel.relative_rect.right - 3 * WIDTH_PADDING),
                                                    int(TEXT_HEIGHT)),
                                manager=manager,
                                container=self,
                                parent_element=self,
                                object_id="edgetagsbox"
                           )
        self.defaultTextColor = self.edgeTagsBox.text_colour

        self.questionMarkButton = UIButton(
                                        relative_rect = pygame.Rect(
                                            int(self.edgeTagsBox.relative_rect.right - QUESTION_BUTTON_WIDTH),
                                            int(self.edgeTagsBox.relative_rect.bottom + HEIGHT_PADDING),
                                            int(QUESTION_BUTTON_WIDTH),
                                            int(QUESTION_BUTTON_HEIGHT),
                                        ),
                                        manager=manager,
                                        container=self,
                                        parent_element=self,
                                        object_id="questionbutton",
                                        text="HELP",
                                    )

        self.tileDetailsPanels = []

        self.messageWindow = None

        self.adjTiles = {}
        self.editButtons = {}
        for direction in self.Dirs():
            self.adjTiles[direction] = None
            self.CreateEditButton(direction)

        self.SubscribeEvents()

    def OpenHelpWindow(self):
        if self.messageWindow:
            self.messageWindow.kill()

        self.messageWindow = adjacencyhelpwindow.AdjacencyHelpWindow(
            rect = pygame.Rect(
                int(0),
                int(0),
                int(self.relative_rect.width),
                int(self.relative_rect.height)
            ),
            manager=self.ui_manager
        )

    def SubscribeEvents(self):
        pub.subscribe(self.OnSelectableTileHovered, "selectabletile.hovered")
        pub.subscribe(self.OnSelectableTileUnhovered, "selectabletile.unhovered")
        pub.subscribe(self.OnTileAdjacencyChanged, "tileseteditor.adjacencychanged")
        pub.subscribe(self.OnTagsChanged, "tileseteditor.tagschanged")

    def OnSelectableTileHovered(self, tilePanel):
        if tilePanel not in self.tileDetailsPanels:
            return
    
        if self.editingDir == None:
            return

        self.SetAdjacentTile(self.editingDir, tilePanel.GetTile())

    def OnSelectableTileUnhovered(self, tilePanel):
        pass

    def CreateEditButton(self, direction):
        self.editButtons[direction] = UIButton(
                                        relative_rect=pygame.Rect(
                                            int(self.GetCenterTileDisplayX() + direction.x * (TILE_DISPLAY_WIDTH * 2 + WIDTH_PADDING)), 
                                            int(self.GetCenterTileDisplayY() + (TILE_DISPLAY_HEIGHT-TEXT_HEIGHT)/2 + direction.y * (TILE_DISPLAY_HEIGHT + TEXT_HEIGHT + 2*HEIGHT_PADDING)), 
                                            int(TILE_DISPLAY_WIDTH),
                                            int(TEXT_HEIGHT)
                                        ),
                                        manager=self.ui_manager,
                                        container=self,
                                        parent_element=self,
                                        object_id="editbutton",
                                        text="Edit"
                                    )

    def SetTile(self, tile, tileset):
        self.tile = tile
        self.tileset = tileset
        self.tileSize = tileset.GetTileSize()
        tileSurface = self.LoadImage(tile)
        self.SetImage(tileSurface)
        
        self.tileDetailsPanels = []
       
        panelY = HEIGHT_PADDING
        for adjacentTileName, adjacentTile in tileset.tiles.items():
            selectableTile = selectabletilepanel.SelectableTilePanel(
                                    rect=pygame.Rect(
                                        int(WIDTH_PADDING),
                                        int(panelY),
                                        int(self.allTilesList.scrolling_width - self.allTilesList.scroll_bar_width - 2 * WIDTH_PADDING),
                                        int(0)
                                    ),
                                    imageLoader=self.imageLoader,
                                    manager=self.ui_manager,
                                    container=self.allTilesList,
                                    element_id="selectabletile",
                                    tile=adjacentTile,
                                    tileset=tileset,
                                    on_toggle_selected=self.RunSetTileAllowAdjacencyCommand,
                                    additionalSelectionText="Prohibit",
                                    on_additional_selected=self.RunSetTileProhibitAdjacencyCommand
                                )
            selectableTile.set_dimensions((selectableTile.rect.width, selectableTile.GetDesiredHeight()))

            self.tileDetailsPanels.append(selectableTile)
            panelY = selectableTile.relative_rect.bottom
        
        # Due to the scrollable container resizing its usable area because it now has a
        # vertical scroll bar, we have to actually call this twice to get the correct width
        # to not create a horizontal scroll bar!
        self.allTilesList.set_scrollable_area_dimensions(
            (self.allTilesList.scrolling_width,
            self.tileDetailsPanels[len(self.tileDetailsPanels)-1].relative_rect.bottom))
        self.allTilesList.set_scrollable_area_dimensions(
            (self.allTilesList.scrolling_width-self.allTilesList.scroll_bar_width, 
            self.allTilesList.scrolling_height))

        for direction in self.Dirs():
            for adjTileID in tile.GetAdjacent(direction):
                adjTile = tileset.GetTile(adjTileID)
                self.SetAdjacentTile(direction, adjTile)
                break

        self.StartEditingDir(self.Dirs()[0])

    def process_event(self, event):
        handled = super().process_event(event)
        if event.type != pygame.USEREVENT:
            return False

        if event.user_type == pygame_gui.UI_BUTTON_PRESSED:
            for direction, dirEditButton in self.editButtons.items():
                if event.ui_element == dirEditButton:
                    self.StartEditingDir(direction)
                    return True
            
            if event.ui_element == self.questionMarkButton:
                self.OpenHelpWindow()
                return True
        
        elif event.user_type == pygame_gui.UI_TEXT_ENTRY_CHANGED:
            self.HandleTextChange(event)
            return True

        elif event.user_type == pygame_gui.UI_TEXT_ENTRY_FINISHED:
            self.HandleTextEntry(event)
            return True

        return False
    
    def HandleTextChange(self, event):
        if event.ui_element == self.edgeTagsBox:
            event.ui_element.text_colour = CHANGED_TEXT_COLOR

    def HandleTextEntry(self, event):
        if event.ui_element == self.edgeTagsBox:
            event.ui_element.text_colour = self.defaultTextColor

        if event.ui_element == self.edgeTagsBox:
            self.RunChangeTagsCommand(event.text)

    def RunChangeTagsCommand(self, text):
        editDir = copy.deepcopy(self.editingDir)
        tagStr = self.StringifyEdgeTags(self.tile.GetEdgeTags(editDir))

        pub.sendMessage("tileseteditor.changetags", tile=self.tile, editDir=editDir, oldTags=tagStr, newTags=text)

    def OnTagsChanged(self, tile, editDir):
        if self.editingDir == editDir and self.tile == tile:
            self.LoadEdgeTags(editDir)

        self.UpdateAdjacencySelections(self.editingDir)

    def RunSetTileAllowAdjacencyCommand(self, otherTileUI, allow=True):
        otherTile = otherTileUI.GetTile()
        dir = copy.deepcopy(self.editingDir)
        oppositeDir = dir.Inverse()
        thisTile = self.tile
        if allow == (otherTile.GetName() in thisTile.GetExplicitAdjacent(dir) or \
            thisTile.GetName() in otherTile.GetExplicitAdjacent(oppositeDir)):
            # Nothing to do.
            return

        prohibitOnUndo = allow and (thisTile.IsAdjacencyProhibited(otherTile.GetName(), dir) or \
                                    otherTile.IsAdjacencyProhibited(thisTile.GetName(), oppositeDir))

        pub.sendMessage("tileseteditor.setallowadjacency", thisTile=thisTile, otherTile=otherTile, dir=dir, allow=allow, prohibitOnUndo=prohibitOnUndo)

    def RunSetTileProhibitAdjacencyCommand(self, otherTileUI, prohibit=True):
        otherTile = otherTileUI.GetTile()
        dir = copy.deepcopy(self.editingDir)
        oppositeDir = dir.Inverse()
        thisTile = self.tile
        if prohibit == (thisTile.IsAdjacencyProhibited(otherTile.GetName(), dir) or \
            otherTile.IsAdjacencyProhibited(thisTile.GetName(), oppositeDir)):
            # Nothing to do.
            return

        currentAllow = (otherTile.GetName() in thisTile.GetExplicitAdjacent(dir) or \
                                    thisTile.GetName() in otherTile.GetExplicitAdjacent(oppositeDir))

        allowOnUndo = prohibit and currentAllow

        pub.sendMessage("tileseteditor.setprohibitadjacency", thisTile=thisTile, otherTile=otherTile, dir=dir, prohibit=prohibit, allowOnUndo=allowOnUndo)

    def OnTileAdjacencyChanged(self, tile, otherTile, dir=None, allow=True, prohibit=False):
        # We may need to recompute all our adjacency info, because it may depend
        # on the tile that changed regardless of which one it was.
        self.UpdateAdjacencySelections(self.editingDir)

    def GetSelectableTile(self, tile):
        for ui_element in self.tileDetailsPanels:
            if ui_element.GetTile().GetName() == tile.GetName():
                return ui_element

        return None

    def GetTile(self):
        return self.tile

    def StartEditingDir(self, direction):
        self.editingDir = direction

        self.UpdateAdjacencySelections(direction)

        for buttonDir in self.editButtons:
            if buttonDir == direction:
                self.editButtons[buttonDir].select()
            else:
                self.editButtons[buttonDir].unselect()

        self.LoadEdgeTags(direction)

    def UpdateAdjacencySelections(self, direction):
        tile = self.GetTile()
        invDir = direction.Inverse()
        for tilePanel in self.tileDetailsPanels:
            adjTile = tilePanel.GetTile()
            if tile.CanBeAdjacent(adjTile.GetName(), direction) or \
                adjTile.CanBeAdjacent(tile.GetName(), invDir):
                if self.TilesHaveSameEdgeTag(direction, invDir, tile, adjTile):
                    tilePanel.SetSelectable(False, additionalSelectable=True)
                    tilePanel.SetCaption("(%s) %s" % (self.GetOverlappingTags(direction, invDir, tile, adjTile), adjTile.GetName()))
                else:
                    tilePanel.SetSelectable(True, additionalSelectable=True)
                    tilePanel.SetCaption(adjTile.GetName())
                tilePanel.SetSelected(True)
            else:
                tilePanel.SetSelectable(True, additionalSelectable=True)
                tilePanel.SetCaption(adjTile.GetName())
                tilePanel.SetSelected(False)

            if tile.IsAdjacencyProhibited(adjTile.GetName(), direction) or \
                adjTile.IsAdjacencyProhibited(tile.GetName(), invDir):
                tilePanel.SetAdditionalSelected(True)
            else:
                tilePanel.SetAdditionalSelected(False)

    def TilesHaveSameEdgeTag(self, dir, invDir, tile, adjTile):
        return len(self.GetOverlappingTags(dir, invDir, tile, adjTile)) > 0
    
    def GetOverlappingTags(self, dir, invDir, tile, adjTile):
        overlappingTagSet = adjTile.GetEdgeTags(invDir).intersection(tile.GetEdgeTags(dir))
        result = ""
        if len(overlappingTagSet) == 0:
            return result

        for tag in overlappingTagSet:
            result += tag + ", "
        
        return result[:-2]
    
    def LoadEdgeTags(self, direction):
        tags = self.tile.GetEdgeTags(direction)
        tagsStr = self.StringifyEdgeTags(tags)

        self.edgeTagsBox.set_text(tagsStr)
        self.edgeTagsBox.text_colour = self.defaultTextColor
        
    def StringifyEdgeTags(self, tags):
        tagsStr = ""
        for tag in tags:
            tagsStr += ("%s, " % tag)
        
        if len(tagsStr) > 0:
            tagsStr = tagsStr[:-2]
        
        return tagsStr

    def Dirs(self):
        return DIRS

    def LoadImage(self, tile):
        loadedImage = self.imageLoader.LoadOrGetImage(tile.GetFile())
        tileSurface = pygame.Surface((int(self.tileSize.x), int(self.tileSize.y)))
        tileSurface.blit(loadedImage, (0,0), (tile.GetPos().x, tile.GetPos().y, self.tileSize.x, self.tileSize.y))
        return tileSurface

    def SetAdjacentTile(self, direction, adjTile):
        if self.adjTiles[direction]:
            self.adjTiles[direction].kill()

        adjTileSurface = self.LoadImage(adjTile)
        
        adjTileImage = UIImage( 
                                relative_rect=pygame.Rect(
                                    int(self.GetCenterTileDisplayX() + direction.x * TILE_DISPLAY_WIDTH), 
                                    int(self.GetCenterTileDisplayY() + direction.y * TILE_DISPLAY_HEIGHT), 
                                    int(TILE_DISPLAY_WIDTH),
                                    int(TILE_DISPLAY_HEIGHT)
                                ),
                                image_surface=adjTileSurface,
                                manager=self.ui_manager, 
                                container=self, 
                                parent_element=self,
                                object_id="tileadjacent"
                            )
        
        self.adjTiles[direction] = adjTileImage

    def SetImage(self, loadedImage):
        if self.tileImage:
            self.tileImage.kill()

        self.tileImage = UIImage( 
                                    relative_rect=pygame.Rect(
                                        int(self.GetCenterTileDisplayX()), 
                                        int(self.GetCenterTileDisplayY()), 
                                        int(TILE_DISPLAY_WIDTH), 
                                        int(TILE_DISPLAY_HEIGHT)
                                    ),
                                    image_surface=loadedImage,
                                    manager=self.ui_manager, 
                                    container=self, 
                                    parent_element=self,
                                    object_id="tileimage"
                                )

    def GetCenterTileDisplayY(self):
        return int(self.edgeTagsBox.relative_rect.bottom + 2*TILE_DISPLAY_HEIGHT + HEIGHT_PADDING)

    def GetCenterTileDisplayX(self):
        return int(self.allTilesList.relative_rect.right + 3 * TILE_DISPLAY_WIDTH)