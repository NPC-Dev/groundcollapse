from pygame_gui.elements.ui_panel import UIPanel
from pygame_gui.elements.ui_label import UILabel
from pygame_gui.elements.ui_button import UIButton
from pygame_gui.elements.ui_scrolling_container import UIScrollingContainer
from pygame_gui.elements.ui_text_entry_line import UITextEntryLine
from pygame_gui.elements.ui_drop_down_menu import UIDropDownMenu
from pygame_gui.windows.ui_file_dialog import UIFileDialog
from groundcollapse.editor import tilesettingswindow
from groundcollapse.editor import spritesheetimporter
from groundcollapse.editor import generalhelpwindow
from groundcollapse.editor.commands import commandqueue
from groundcollapse.editor.commands import settextcommand
from groundcollapse.editor.commands import twofunctioncommand
from pubsub import pub
import pygame
import pygame_gui
import os

from groundcollapse.editor import tiledetailpanel

WIDTH_PADDING=25
HEIGHT_PADDING=20
TOP_MENU_HEIGHT=50
TEXT_HEIGHT=25
FILE_MENU_WIDTH=175
EDIT_MENU_WIDTH=175
HELP_MENU_WIDTH=175
EDIT_BUTTON_WIDTH = 100
ADD_TILE_BUTTON_WIDTH = 200
ADD_TILE_BUTTON_HEIGHT = 2 * TEXT_HEIGHT

ERROR_COLOR = (255,0,0)
CHANGED_TEXT_COLOR = (255, 255, 180)

class TilesetEditor(UIPanel):
    def __init__(self, rect, manager, tilesetLoader, element_id="tileseteditor", tileset=None, tileExtraDataFactory = None):
        UIPanel.__init__(self, relative_rect=rect, starting_layer_height=1, manager=manager, element_id=element_id)
        self.loadedImages = {}

        self.SubscribeEvents()

        self.tilesetLoader = tilesetLoader
        self.tileExtraDataFactory = tileExtraDataFactory

        self.commandqueue = commandqueue.CommandQueue()

        self.fileMenu = UIDropDownMenu(
                            relative_rect=pygame.Rect(
                                int(WIDTH_PADDING),
                                int(HEIGHT_PADDING),
                                int(FILE_MENU_WIDTH),
                                int(TEXT_HEIGHT)
                            ),
                            options_list=["File","New","Open","Save (Ctrl+S)","Save As...","Generate","Main Menu","Quit (Ctrl+Q)"],
                            starting_option="File",
                            manager=manager,
                            container=self,
                            object_id="filemenu"
                        )
        
        self.editMenu = UIDropDownMenu(
                            relative_rect=pygame.Rect(
                                int(self.fileMenu.relative_rect.right + WIDTH_PADDING),
                                int(HEIGHT_PADDING),
                                int(EDIT_MENU_WIDTH),
                                int(TEXT_HEIGHT)
                            ),
                            options_list=["Edit", "Undo (Ctrl+Z)", "Redo (Ctrl+Y)", "Add Sprite Sheet"],
                            starting_option="Edit",
                            manager=manager,
                            container=self,
                            object_id="editmenu"
                        )

        self.helpMenu = UIDropDownMenu(
                           relative_rect=pygame.Rect(
                               int(self.editMenu.relative_rect.right + WIDTH_PADDING),
                               int(HEIGHT_PADDING),
                               int(HELP_MENU_WIDTH),
                               int(TEXT_HEIGHT)
                           ),
                           options_list=["Help","General Usage"],
                           starting_option="Help",
                           manager=manager,
                           container=self,
                           object_id="helpmenu"
                        )

        self.tilesetPathLabel = UILabel(
                                    relative_rect=pygame.Rect(
                                        int(WIDTH_PADDING), 
                                        int(TOP_MENU_HEIGHT + HEIGHT_PADDING), 
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)), 
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Tileset File", 
                                    manager=manager, 
                                    container=self
                                )

        self.tilesetPathButton = UIButton(
                                    relative_rect=pygame.Rect(
                                        int(self.tilesetPathLabel.relative_rect.right + WIDTH_PADDING),
                                        int(self.tilesetPathLabel.relative_rect.top),
                                        int(EDIT_BUTTON_WIDTH),
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Pick",
                                    manager=manager, 
                                    container=self, 
                                    parent_element=self, 
                                    object_id="imagepathbutton"
                                )

        self.tilesetPathBox = UITextEntryLine(
                                    relative_rect=pygame.Rect(
                                        int(self.tilesetPathButton.relative_rect.right + WIDTH_PADDING),
                                        int(self.tilesetPathButton.relative_rect.top),
                                        int(self.rect.width - self.tilesetPathButton.relative_rect.right - 2 * WIDTH_PADDING),
                                        int(TEXT_HEIGHT)
                                    ), 
                                    manager=manager, 
                                    container=self, 
                                    parent_element=self, 
                                    object_id="imagepath"
                                )
                                    
        self.tilesetPathBox.set_text("File Path")

        self.defaultTextColor = self.tilesetPathBox.text_colour

        self.tileWidthLabel = UILabel(
                                    relative_rect=pygame.Rect(
                                        int(WIDTH_PADDING), 
                                        int(self.tilesetPathBox.relative_rect.bottom + HEIGHT_PADDING), 
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)), 
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Tile Width", 
                                    manager=manager, 
                                    container=self
                                )
                                
        self.tileWidthTextBox = UITextEntryLine(
                                    relative_rect=pygame.Rect(
                                        int(self.tileWidthLabel.relative_rect.right + WIDTH_PADDING),
                                        int(self.tileWidthLabel.relative_rect.top),
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)),
                                        int(TEXT_HEIGHT)
                                    ), 
                                    manager=manager, 
                                    container=self,
                                    parent_element=self, 
                                    object_id="tilewidthbox"
                                )

        self.tileHeightLabel = UILabel(
                                    relative_rect=pygame.Rect(
                                        int(self.tileWidthTextBox.relative_rect.right + WIDTH_PADDING), 
                                        int(self.tilesetPathBox.relative_rect.bottom + HEIGHT_PADDING), 
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)), 
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Tile Height", 
                                    manager=manager, 
                                    container=self
                                )
                                
        self.tileHeightTextBox = UITextEntryLine(
                                    relative_rect=pygame.Rect(
                                        int(self.tileHeightLabel.relative_rect.right + WIDTH_PADDING),
                                        int(self.tileHeightLabel.relative_rect.top),
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)),
                                        int(TEXT_HEIGHT)
                                    ), 
                                    manager=manager, 
                                    container=self, 
                                    parent_element=self, 
                                    object_id="tileheightbox"
                                )
        
        self.addTileButton = UIButton(
                                    relative_rect=pygame.Rect(
                                        int(self.relative_rect.width / 2 - ADD_TILE_BUTTON_WIDTH / 2),
                                        int(self.tileHeightLabel.relative_rect.bottom + HEIGHT_PADDING),
                                        int(ADD_TILE_BUTTON_WIDTH),
                                        int(ADD_TILE_BUTTON_HEIGHT)
                                    ), 
                                    text="+ New Tile",
                                    manager=manager, 
                                    container=self, 
                                    parent_element=self, 
                                    object_id="addtilebutton"
                                )

        self.messageWindow = None

        self.tileDetailsPanels = []
        self.tileDetailsContainer = None

        self.tileSettingsWindow = None
        self.fileDialog = None
        self.spriteSheetImporter = None

        self.filePickedEvent = None

        if None != tileset:
            self.SetTileset(tileset)
        else:
            self.SetTileset(self.tilesetLoader.CreateEmptyTileset("untitledtileset.json"))


    def SubscribeEvents(self):
        pub.subscribe(self.ModeChanged, "mode.changed")
        pub.subscribe(self.OpenTileSettings, "tileseteditor.edit_tile_settings")
        pub.subscribe(self.CreateFilePicker, "tileseteditor.pick_file")
        pub.subscribe(self.RunDeleteTileCommand, "tileseteditor.delete_tile")
        pub.subscribe(self.DuplicateTile, "tileseteditor.duplicate_tile")
        pub.subscribe(self.OnTileAdded, "tileseteditor.tileadded")
        pub.subscribe(self.RunCommand, "tileseteditor.run")
        pub.subscribe(self.RunSetAllowAdjacencyCommand, "tileseteditor.setallowadjacency")
        pub.subscribe(self.RunSetProhibitAdjacencyCommand, "tileseteditor.setprohibitadjacency")
        pub.subscribe(self.RunChangeTagsCommand, "tileseteditor.changetags")
        pub.subscribe(self.RunSetWalkabilityCommand, "tileseteditor.setwalkability")

    def RunCommand(self, command):
        self.commandqueue.Run(command)

    def Undo(self):
        self.commandqueue.Undo()

    def Redo(self):
        self.commandqueue.Redo()

    def LoadTileset(self, path):
        tileset = self.tilesetLoader.LoadTileset(path, self.tileExtraDataFactory)
        self.SetTileset(tileset)

    def SetTileset(self, tileset):
        self.tileset = tileset
        self.OnTilesetPathChanged()

        self.OnTileSizeChanged()

        self.CreateTileDetailsPanels()

    def CreateTileDetailsPanels(self):
        if self.tileDetailsPanels:
            for panel in self.tileDetailsPanels:
                panel.kill()

        if self.tileDetailsContainer:
            self.tileDetailsContainer.kill()

        self.tileDetailsContainer = UIScrollingContainer(
                                        relative_rect=pygame.Rect(
                                            int(self.rect.x + WIDTH_PADDING),
                                            int(self.addTileButton.relative_rect.bottom + HEIGHT_PADDING),
                                            int(self.rect.width - 2*WIDTH_PADDING),
                                            int(self.rect.height - self.addTileButton.relative_rect.bottom - HEIGHT_PADDING * 2)
                                        ), 
                                        manager=self.ui_manager, 
                                        container=self, 
                                        parent_element=self, 
                                        object_id="tiledetaillist"
                                    )
       
        self.tileDetailsPanels = []
        panelY = HEIGHT_PADDING
        for tilename, tile in self.tileset.tiles.items():
            self.AddTilePanel(tile)

        self.ResizeTileDetailsContainer()

    def AddTilePanel(self, tile, index=-1):
        if index < 0 or index > len(self.tileDetailsPanels):
            index = len(self.tileDetailsPanels)

        panelY = HEIGHT_PADDING
        if index > 0:
            panelY = self.tileDetailsPanels[index-1].relative_rect.bottom

        tileDetailPanel = tiledetailpanel.TileDetailPanel(
                                rect=pygame.Rect(
                                    int(WIDTH_PADDING),
                                    int(panelY),
                                    int(self.tileDetailsContainer.relative_rect.width - 3 * WIDTH_PADDING),
                                    int(200)
                                ),
                                imageLoader=self,
                                manager=self.ui_manager,
                                container=self.tileDetailsContainer,
                                element_id="tiledetail",
                                tile=tile,
                                tileset=self.tileset
                            )
        
        panelHeight = tileDetailPanel.GetDesiredHeight()
        tileDetailPanel.set_dimensions((tileDetailPanel.rect.width, panelHeight))

        self.tileDetailsPanels.insert(index, tileDetailPanel)
        nextPanel = index+1

        for panelIdx in range(nextPanel, len(self.tileDetailsPanels)):
            panel = self.tileDetailsPanels[panelIdx]
            panel.set_relative_position(panel.relative_rect.topleft + pygame.Vector2(0, panelHeight))

    def ResizeTileDetailsContainer(self):
        # Due to the scrollable container resizing its usable area because it now has a
        # vertical scroll bar, we have to actually call this twice to get the correct width
        # to not create a horizontal scroll bar!
        if len(self.tileDetailsPanels) > 0:
            self.tileDetailsContainer.set_scrollable_area_dimensions(
                (self.tileDetailsContainer.scrolling_width,
                self.tileDetailsPanels[len(self.tileDetailsPanels)-1].relative_rect.bottom))
            self.tileDetailsContainer.set_scrollable_area_dimensions(
                (self.tileDetailsContainer.scrolling_width-self.tileDetailsContainer.scroll_bar_width, 
                self.tileDetailsContainer.scrolling_height))

    def process_event(self, event):
        handled = super().process_event(event)

        if event.type == pygame.KEYUP:
            if event.mod & pygame.KMOD_CTRL:
                if event.key == pygame.K_z:
                    self.Undo()
                    return True
                elif event.key == pygame.K_y:
                    self.Redo()
                    return True
                elif event.key == pygame.K_s:
                    self.SaveTileset()
                    return True
                elif event.key == pygame.K_q:
                    pub.sendMessage("quit")
                    return True
        
        if event.type == pygame.USEREVENT:
            if event.user_type == pygame_gui.UI_DROP_DOWN_MENU_CHANGED:
                if event.ui_element == self.fileMenu:
                    self.OnFileMenuClicked(event)
                    return True
                elif event.ui_element == self.editMenu:
                    self.OnEditMenuClicked(event)
                    return True
                elif event.ui_element == self.helpMenu:
                    self.OnHelpMenuClicked(event)
                    return True

            elif event.user_type == pygame_gui.UI_FILE_DIALOG_PATH_PICKED:
                if event.ui_element == self.fileDialog:
                    self.OnFilePicked(event)
                    return True

            elif event.user_type == pygame_gui.UI_TEXT_ENTRY_CHANGED:
                self.HandleTextChange(event)
                return True

            elif event.user_type == pygame_gui.UI_TEXT_ENTRY_FINISHED:
                self.HandleTextEntry(event)
                return True

            elif event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                if event.ui_element == self.tilesetPathButton:
                    self.CreateFilePicker(self.tileset.GetFilePath(), self.ChangeTilesetPath);
                    return True
                elif event.ui_element == self.addTileButton:
                    self.RunCreateTileCommand()
                    return True

        return handled
    
    def HandleTextChange(self, event):
        if event.ui_element == self.tilesetPathBox or \
            event.ui_element == self.tileWidthTextBox or \
            event.ui_element == self.tileHeightTextBox:
            event.ui_element.text_colour = CHANGED_TEXT_COLOR

    def HandleTextEntry(self, event):
        if event.ui_element == self.tilesetPathBox or \
            event.ui_element == self.tileWidthTextBox or \
            event.ui_element == self.tileHeightTextBox:
            event.ui_element.text_colour = self.defaultTextColor
       
        if event.ui_element == self.tilesetPathBox:
            self.RunSetFilepathCommand(event.text)
        elif event.ui_element == self.tileWidthTextBox:
            self.RunChangeTileSizeXCommand(event.text)
        elif event.ui_element == self.tileHeightTextBox:
            self.RunChangeTileSizeYCommand(event.text)

    def OnFilePicked(self, event):
        if self.filePickedEvent == None:
            print("ERROR: No event to call when file is picked?")
            return False

        abspath = pygame_gui.core.utility.create_resource_path(event.text)
        selectedPath = os.path.relpath(abspath, os.getcwd())
        self.filePickedEvent(selectedPath)

    def OnFileMenuClicked(self, event):
        if event.text == "Save (Ctrl+S)":
            self.SaveTileset()

        elif event.text == "Quit (Ctrl+Q)":
            pub.sendMessage("quit");
            return

        elif event.text == "Save As...":
            self.CreateFilePicker(self.tileset.GetFilePath(), self.SaveTileset)

        elif event.text == "Main Menu":
            pub.sendMessage("mode.changed", mode="mainmenu")
        
        elif event.text == "Generate":
            pub.sendMessage("mode.changed", mode="levelviewer")

        elif event.text == "Open":
            self.CreateFilePicker(self.tileset.GetFilePath(), self.LoadTileset)

        elif event.text == "New":
            self.CreateFilePicker(self.tileset.GetFilePath(), self.StartNewTileset)

        # This is a hack to get the menu to return to showing the first option.
        # This could probably be encapsulated in a subclass of DropdownMenu
        self.fileMenu.selected_option = "File"
        self.fileMenu.current_state.selected_option = "File"
        self.fileMenu.current_state.finish()
        self.fileMenu.current_state.start()

    def OnEditMenuClicked(self, event):
        if event.text == "Add Sprite Sheet":
            self.OpenSpriteSheetImporter()
        elif event.text == "Undo (Ctrl+Z)":
            if self.commandqueue.CanUndo():
                self.Undo()
        elif event.text == "Redo (Ctrl+Y)":
            if self.commandqueue.CanRedo():
                self.Redo()

        # This is a hack to get the menu to return to showing the first option.
        # This could probably be encapsulated in a subclass of DropdownMenu
        self.editMenu.selected_option = "Edit"
        self.editMenu.current_state.selected_option = "Edit"
        self.editMenu.current_state.finish()
        self.editMenu.current_state.start()

    def OnHelpMenuClicked(self, event):
        if event.text == "General Usage":
            self.OpenHelpWindow()

        # This is a hack to get the menu to return to showing the first option.
        # This could probably be encapsulated in a subclass of DropdownMenu
        self.helpMenu.selected_option = "Help"
        self.helpMenu.current_state.selected_option = "Help"
        self.helpMenu.current_state.finish()
        self.helpMenu.current_state.start()

    def StartNewTileset(self, path):
        tileset = self.tilesetLoader.CreateEmptyTileset(path)
        self.SetTileset(tileset)

    def OnTilesetPathChanged(self):
        self.tilesetPathBox.set_text(self.tileset.GetFilePath())
    
    def RunSetFilepathCommand(self, path):
        if self.tileset.GetFilePath() == path:
            # Don't bother running a command that won't actually change anything.
            return
        command = settextcommand.SetTextCommand(self.tileset.GetFilePath(), path, self.ChangeTilesetPath)
        self.RunCommand(command)

    def ChangeTilesetPath(self, path):
        self.tileset.SetFilePath(path)
        self.OnTilesetPathChanged()

    def OnTileSizeChanged(self):
        tileSize = self.tileset.GetTileSize()
        self.tileWidthTextBox.set_text(str(tileSize.x))
        self.tileHeightTextBox.set_text(str(tileSize.y))
    
    def RunChangeTileSizeXCommand(self, text):
        oldText = str(self.tileset.GetTileSize().x)
        if oldText == text:
            # Don't bother running a command that won't actually change anything.
            return
        command = settextcommand.SetTextCommand(oldText, text, self.ChangeTileSizeX)
        self.RunCommand(command)
    
    def RunChangeTileSizeYCommand(self, text):
        oldText = str(self.tileset.GetTileSize().y)
        if oldText == text:
            # Don't bother running a command that won't actually change anything.
            return
        command = settextcommand.SetTextCommand(oldText, text, self.ChangeTileSizeY)
        self.RunCommand(command)

    def ChangeTileSizeX(self, x):
        try:
            self.tileWidthTextBox.set_text(x)
            self.tileset.SetTileSize(int(x), self.tileset.GetTileSize().y)
            self.tileWidthTextBox.text_colour = self.defaultTextColor
            self.CreateTileDetailsPanels()
        except ValueError:
            self.tileWidthTextBox.text_colour = ERROR_COLOR
            return

    def ChangeTileSizeY(self, y):
        try:
            self.tileHeightTextBox.set_text(y)
            self.tileset.SetTileSize(self.tileset.GetTileSize().x, int(y))
            self.tileHeightTextBox.text_colour = self.defaultTextColor
            self.CreateTileDetailsPanels()
        except ValueError:
            self.tileHeightTextBox.text_colour = ERROR_COLOR
            return

    def SaveTileset(self, path = None):
        if path == None:
            path = self.tileset.GetFilePath()

        self.tileset.SetFilePath(path)
        self.OnTilesetPathChanged()
        tilesetJson = self.tileset.ToJson()
        with open(path, "w") as tilesetFile:
            tilesetFile.write(tilesetJson)

    def OpenTileSettings(self, tile):
        if self.tileSettingsWindow != None:
            self.tileSettingsWindow.kill()

        self.tileSettingsWindow = tilesettingswindow.TileSettingsWindow(
                                    rect=pygame.Rect(
                                        WIDTH_PADDING * 4,
                                        HEIGHT_PADDING * 4,
                                        self.rect.width - 8 * WIDTH_PADDING,
                                        self.rect.height - 8 * HEIGHT_PADDING), 
                                    manager=self.ui_manager, 
                                    imageLoader=self, tile=tile, 
                                    tileset=self.tileset)

    def OpenSpriteSheetImporter(self):
        if self.spriteSheetImporter != None:
            self.spriteSheetImporter.kill()

        self.spriteSheetImporter = spritesheetimporter.SpriteSheetImporter(
                                    rect=pygame.Rect(
                                        WIDTH_PADDING * 4,
                                        HEIGHT_PADDING * 4,
                                        self.rect.width - 8 * WIDTH_PADDING,
                                        self.rect.height - 8 * HEIGHT_PADDING), 
                                    manager=self.ui_manager, 
                                    editor=self,
                                    imageLoader=self, 
                                    tileset=self.tileset)

    def Tick(self, tickDiff, display):
        pass

    def DeleteTile(self, tile):
        removedTile = self.tileset.RemoveTile(tile.GetName())

        panelToDelete = None
        for panelIdx in range(0, len(self.tileDetailsPanels)):
            tileDetailPanel = self.tileDetailsPanels[panelIdx]; 
            if tileDetailPanel.GetTile() == tile:
                panelToDelete = tileDetailPanel
                panelHeight = tileDetailPanel.rect.height
                nextPanel = panelIdx
                break

        panelToDelete.kill()
        self.tileDetailsPanels.remove(panelToDelete)

        for panelIdx in range(nextPanel, len(self.tileDetailsPanels)):
            panel = self.tileDetailsPanels[panelIdx]
            panel.set_relative_position(panel.relative_rect.topleft - pygame.Vector2(0, panelHeight))

        self.ResizeTileDetailsContainer()

        return removedTile

    def RunCreateTileCommand(self):
        newName = self.tileset.CreateUniqueTilename()
        command = twofunctioncommand.TwoFunctionCommand(lambda : self.CreateTile(newName), self.DeleteTile)
        self.RunCommand(command)
    
    def RunDeleteTileCommand(self, tile):
        index = -1
        for idx in range(0, len(self.tileDetailsPanels)):
            if self.tileDetailsPanels[idx].GetTile() == tile:
                index = idx
                break

        if index == -1:
            return

        command = twofunctioncommand.TwoFunctionCommand(lambda : self.DeleteTile(tile), lambda tile : self.AddTile(tile, index))
        self.RunCommand(command)

    def CreateTile(self, tileName=None):
        newTile = self.tileset.CreateEmptyTile(tileName)
        self.OnTileAdded(newTile)
        return newTile

    def AddTile(self, tile, index=-1):
        self.tileset.AddTile(tile)
        self.OnTileAdded(tile, index)

    def OnTileAdded(self, tile, index=-1):
        self.AddTilePanel(tile, index)

        self.ResizeTileDetailsContainer()

    def DuplicateTile(self, tile):
        newTile = self.tileset.DuplicateTile(tile)
        self.AddTilePanel(newTile)

        self.ResizeTileDetailsContainer()

    def ModeChanged(self, mode):
        if mode == "tileseteditor":
            self.show()
        else:
            self.hide()

    def LoadOrGetImage(self, filename):
        image = self.loadedImages.get(filename)
        if image:
            return image

        try:
            image = pygame.image.load(filename)
            image = image.convert()

            self.loadedImages[filename] = image
            return image
        except Exception as e:
            print(e)
            return None

    def CreateFilePicker(self, initialPath, filePickedEvent, allow_existing_only = False, allow_dirs = False):
        if (self.fileDialog != None):
            self.fileDialog.kill()
        
        self.filePickedEvent = filePickedEvent

        self.fileDialog = UIFileDialog(rect = pygame.Rect(
                                            self.rect.left + WIDTH_PADDING,
                                            self.rect.top + HEIGHT_PADDING,
                                            self.rect.width - 2 * WIDTH_PADDING,
                                            self.rect.height - 2 * HEIGHT_PADDING,
                                        ), 
                                        manager = self.ui_manager, 
                                        window_title = "Choose a File",
                                        initial_file_path = initialPath, 
                                        object_id = '#file_dialog',
                                        allow_existing_files_only = allow_existing_only, 
                                        allow_picking_directories = allow_dirs, 
                                        visible = 1)

    def RunSetProhibitAdjacencyCommand(self, thisTile, otherTile, dir, prohibit, allowOnUndo):
        command = twofunctioncommand.TwoFunctionCommand(lambda : self.SetTileAllowAdjacency(thisTile, otherTile, dir, allow=False, prohibit=prohibit),
                                                        lambda otherTile : self.SetTileAllowAdjacency(thisTile, otherTile, dir, allowOnUndo, not prohibit))
        self.RunCommand(command)

    def RunSetAllowAdjacencyCommand(self, thisTile, otherTile, dir, allow, prohibitOnUndo):
        command = twofunctioncommand.TwoFunctionCommand(lambda : self.SetTileAllowAdjacency(thisTile, otherTile, dir, allow),
                                                        lambda otherTile : self.SetTileAllowAdjacency(thisTile, otherTile, dir, not allow, prohibitOnUndo))
        self.RunCommand(command)

    def SetTileAllowAdjacency(self, tile, otherTile, dir=None, allow=True, prohibit=False):
        if allow and prohibit:
            raise ValueError("Allow and prohibit adjacency can't both be true!")

        oppositeDir = dir.Inverse()
        if allow:
            tile.AllowAdjacency(otherTile.GetName(), dir)
            otherTile.AllowAdjacency(tile.GetName(), oppositeDir)
        else:
            if prohibit:
                tile.ProhibitAdjacency(otherTile.GetName(), dir)
                otherTile.ProhibitAdjacency(tile.GetName(), oppositeDir)
            else:
                tile.RemoveExplicitAdjacency(otherTile.GetName(), dir)
                otherTile.RemoveExplicitAdjacency(tile.GetName(), oppositeDir)
                tile.RemoveProhibitedAdjacency(otherTile.GetName(), dir)
                otherTile.RemoveProhibitedAdjacency(tile.GetName(), oppositeDir)

        self.tileset.RecomputeAdjacencies()
        pub.sendMessage("tileseteditor.adjacencychanged", tile=tile, otherTile=otherTile, dir=dir, allow=allow, prohibit=prohibit)
 
        return otherTile

    def RunChangeTagsCommand(self, tile, editDir, oldTags, newTags):
        newTagsSet = self.TagifyStr(newTags)
        if (tile.GetEdgeTags(editDir) == set(newTagsSet)):
            # Don't bother running command if it won't do anything
            return
        
        command = settextcommand.SetTextCommand(oldTags, newTags, lambda text: self.ChangeTags(tile, editDir, text))
        self.RunCommand(command)

    def ChangeTags(self, tile, editDir, text):
        tags = self.TagifyStr(text)
        tile.SetEdgeTags(editDir, tags)
        self.tileset.RecomputeAdjacencies()
        pub.sendMessage("tileseteditor.tagschanged", tile=tile, editDir=editDir)

    def TagifyStr(self, text):
        tags = text.split(",")
        tags = [tag.strip() for tag in tags if len(tag) > 0] # Remove any empty tags and strip spaces
        return tags

    def RunSetWalkabilityCommand(self, tile, dir, walkable):
        if tile.CanExit(dir) == walkable:
            return
        
        command = twofunctioncommand.TwoFunctionCommand(lambda: self.SetWalkability(tile, dir, walkable), lambda tileToSet: self.SetWalkability(tileToSet, dir, not walkable))
        self.RunCommand(command)

    def SetWalkability(self, tile, dir, walkable):
        tile.SetCanExit(dir, walkable)

        pub.sendMessage("tileseteditor.walkabilitychanged", tile=tile, dir=dir)
        return tile

    def OpenHelpWindow(self):
        if self.messageWindow:
            self.messageWindow.kill()

        self.messageWindow = generalhelpwindow.GeneralHelpWindow(
            rect = pygame.Rect(
                int(0),
                int(0),
                int(self.relative_rect.width),
                int(self.relative_rect.height)
            ),
            manager=self.ui_manager
        )

