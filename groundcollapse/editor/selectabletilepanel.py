import pygame
from pubsub import pub
from pygame_gui.elements.ui_panel import UIPanel
from pygame_gui.elements.ui_button import UIButton
from pygame_gui.elements.ui_image import UIImage
from pygame_gui.elements.ui_text_entry_line import UITextEntryLine
from groundcollapse import tileset
import pygame_gui

WIDTH_PADDING=15
HEIGHT_PADDING=15
TEXT_HEIGHT=30
TILE_DISPLAY_WIDTH=64
TILE_DISPLAY_HEIGHT=64

class SelectableTilePanel(UIPanel):
    def __init__(self, 
                rect, 
                manager, 
                imageLoader,
                element_id="selectabletile",
                container=None,
                tile=None,
                tileset=None,
                on_toggle_selected=None,
                additionalSelectionText=None,
                on_additional_selected=None
                ):
        UIPanel.__init__(self, relative_rect=rect, starting_layer_height=1, manager=manager, container=container, element_id=element_id)

        self.imageLoader = imageLoader
        self.tileSize = tileset.GetTileSize()

        self.tileSelectButton = UIButton(
                                    relative_rect=pygame.Rect(
                                        int(WIDTH_PADDING), 
                                        int(HEIGHT_PADDING), 
                                        int(self.rect.width - (2 * WIDTH_PADDING)), 
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Name", 
                                    manager=manager, 
                                    container=self
                                )
        
        self.additionalSelectionButton = None
        if additionalSelectionText != None:
            self.additionalSelectionButton = UIButton(
                                        relative_rect=pygame.Rect(
                                            int(WIDTH_PADDING), 
                                            int(TILE_DISPLAY_HEIGHT + self.tileSelectButton.relative_rect.bottom + 2 * HEIGHT_PADDING), 
                                            int(self.rect.width - (2 * WIDTH_PADDING)), 
                                            int(TEXT_HEIGHT)
                                        ), 
                                        text=additionalSelectionText, 
                                        manager=manager, 
                                        container=self
                                    )

        self.tileImage = None

        self.onToggleSelected = on_toggle_selected

        self.onAdditionalSelected = on_additional_selected
   
        if tile != None:
            self.SetTile(tile, tileset)

    def on_hovered(self):
        pub.sendMessage("selectabletile.hovered", tilePanel=self)

    def on_unhovered(self):
        pub.sendMessage("selectabletile.unhovered", tilePanel=self)

    def process_event(self, event):
        handled = super().process_event(event)
        if event.type != pygame.USEREVENT:
            return False

        if event.user_type == pygame_gui.UI_BUTTON_PRESSED:
            if event.ui_element == self.tileSelectButton:
                self.Toggle()
            elif event.ui_element == self.additionalSelectionButton:
                self.ToggleAdditionalSelection()

    def Toggle(self):
        if self.tileSelectButton.is_selected:
            self.SetSelected(False)
        else:
            self.SetSelected(True)

        if self.onToggleSelected is not None:
            self.onToggleSelected(self, self.tileSelectButton.is_selected)

    def ToggleAdditionalSelection(self):
        if self.additionalSelectionButton.is_selected:
            self.SetAdditionalSelected(False)
        else:
            self.SetAdditionalSelected(True)

        if self.onAdditionalSelected is not None:
            self.onAdditionalSelected(self, self.additionalSelectionButton.is_selected)

    def SetCaption(self, caption):
        self.tileSelectButton.set_text(caption)

    def SetAdditionalSelected(self, selected):
        if selected:
            self.additionalSelectionButton.select()
        else:
            self.additionalSelectionButton.unselect()

    def GetDesiredHeight(self):
        bottomPos = self.tileImage.relative_rect.bottom
        if self.additionalSelectionButton:
            bottomPos = self.additionalSelectionButton.relative_rect.bottom
        
        return bottomPos + HEIGHT_PADDING

    def SetSelectable(self, selectable, additionalSelectable=None):
        if additionalSelectable is None:
            additionalSelectable = selectable

        if not selectable:
            self.tileSelectButton.disable()
        else:
            self.tileSelectButton.enable()
        
        if not additionalSelectable:
            self.additionalSelectionButton.disable()
        else:
            self.additionalSelectionButton.enable()

    def SetTile(self, tile, tileset):
        self.tile = tile
        self.tileSelectButton.set_text(tile.GetName())
       
        tileSurface = self.LoadImage(tile)
        self.SetImage(tileSurface)

    def GetTile(self):
        return self.tile

    def LoadImage(self, tile):
        loadedImage = self.imageLoader.LoadOrGetImage(tile.GetFile())
        tileSurface = pygame.Surface((int(self.tileSize.x), int(self.tileSize.y)))
        tileSurface.blit(loadedImage, (0,0), (tile.GetPos().x, tile.GetPos().y, self.tileSize.x, self.tileSize.y))
        return tileSurface
    
    def SetImage(self, loadedImage):
        if self.tileImage:
            self.tileImage.kill()

        self.tileImage = UIImage( 
                                    relative_rect=pygame.Rect(
                                        int(self.relative_rect.width/2 - TILE_DISPLAY_WIDTH/2),
                                        int(self.tileSelectButton.relative_rect.bottom + HEIGHT_PADDING),
                                        int(TILE_DISPLAY_WIDTH),
                                        int(TILE_DISPLAY_HEIGHT)
                                    ),
                                    image_surface=loadedImage,
                                    manager=self.ui_manager, 
                                    container=self, 
                                    parent_element=self,
                                    object_id="tileimage"
                                )
    
    def SetSelected(self, selected):
        if selected:
            self.tileSelectButton.select()
        else:
            self.tileSelectButton.unselect()
