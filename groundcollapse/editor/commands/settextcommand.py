from groundcollapse.editor.commands import editorcommand

class SetTextCommand(editorcommand.EditorCommand):
    def __init__(self, oldText, newText, runFunc):
        editorcommand.EditorCommand.__init__(self)
        self.oldText = oldText
        self.newText = newText
        self.runFunc = runFunc

    def Run(self):
        editorcommand.EditorCommand.Run(self)
        self.runFunc(self.newText)

    def Undo(self):
        editorcommand.EditorCommand.Undo(self)
        self.runFunc(self.oldText)