from groundcollapse.editor.commands import editorcommand

# Runs the runFunc and saves the result.
# Then, to undo, runs the undoFunc with the result of the runFunc.
class TwoFunctionCommand(editorcommand.EditorCommand):
    def __init__(self, runFunc, undoFunc):
        editorcommand.EditorCommand.__init__(self)
        self.runFunc = runFunc
        self.undoFunc = undoFunc
        self.result = None

    def Run(self):
        editorcommand.EditorCommand.Run(self)
        self.result = self.runFunc()

    def Undo(self):
        editorcommand.EditorCommand.Undo(self)
        self.undoFunc(self.result)