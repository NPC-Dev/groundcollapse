import queue

class CommandQueue:
    def __init__(self):
        self.commands = queue.LifoQueue()
        self.undoneCommands = queue.LifoQueue()

    def Run(self, command, clearRedoStack = True):
        self.commands.put(command)
        if clearRedoStack:
            self.undoneCommands = queue.LifoQueue()
        command.Run()

    def Undo(self):
        if not self.CanUndo():
            return

        command = self.commands.get(block=False)
        self.undoneCommands.put(command)
        command.Undo()

    def Redo(self):
        if not self.CanRedo():
            return

        command = self.undoneCommands.get(block=False)
        if None == command:
            return
        self.Run(command, clearRedoStack=False)

    def CanUndo(self):
        return not self.commands.empty()

    def CanRedo(self):
        return not self.undoneCommands.empty()

    def Clear(self):
        self.commands = queue.LifoQueue()
        self.undoneCommands = queue.LifoQueue()