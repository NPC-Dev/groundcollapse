import pygame
import pygame_gui
from pubsub import pub
from pygame_gui.elements.ui_panel import UIPanel
from pygame_gui.elements.ui_label import UILabel
from pygame_gui.elements.ui_image import UIImage
from pygame_gui.elements.ui_button import UIButton
from groundcollapse import tileset

WIDTH_PADDING=25
HEIGHT_PADDING=20
TEXT_HEIGHT=25
TILE_DISPLAY_WIDTH=64
TILE_DISPLAY_HEIGHT=64
TOOLBAR_HEIGHT = 50
EXIT_BUTTON_WIDTH=100

DIRS = (tileset.Dir(-1,0), tileset.Dir(1,0), tileset.Dir(0,-1), tileset.Dir(0,1), tileset.Dir(-1,-1), tileset.Dir(1,1), tileset.Dir(-1,1), tileset.Dir(1,-1))

class TileWalkabilityPanel(UIPanel):
    def __init__(self, 
                rect, 
                manager, 
                imageLoader,
                element_id="tileadjacency",
                container=None,
                ):
        UIPanel.__init__(self, relative_rect=rect, starting_layer_height=1, anchors={'top': 'top', 'bottom': 'bottom', 'left': 'left', 'right': 'right'}, manager=manager, container=container, element_id=element_id)

        self.SubscribeEvents()

        self.imageLoader = imageLoader

        self.tileImage = None

        self.titleLabel = UILabel(
                                    relative_rect=pygame.Rect(
                                                    int(WIDTH_PADDING),
                                                    int(HEIGHT_PADDING),
                                                    int(self.rect.width - 2 * WIDTH_PADDING),
                                                    int(TEXT_HEIGHT)
                                                ),
                                    manager=manager,
                                    container=self,
                                    parent_element=self,
                                    text="Walkability Settings",
                                    object_id="walkabilitytitle"
                                )

        self.editButtons = {}
        for direction in DIRS:
            self.CreateEditButton(direction)
        
    def SubscribeEvents(self):
        pub.subscribe(self.UpdateWalkability, "tileseteditor.walkabilitychanged")

    def process_event(self, event):
        handled = super().process_event(event)
        if event.type != pygame.USEREVENT:
            return False

        if event.user_type == pygame_gui.UI_BUTTON_PRESSED:
            for direction, dirEditButton in self.editButtons.items():
                if event.ui_element == dirEditButton:
                    self.RunSetWalkabilityCommand(self.tile, direction, walkable=not self.tile.CanExit(direction))
                    return True

        return handled
 
    def CreateEditButton(self, direction):
        self.editButtons[direction] = UIButton(
                                        relative_rect=pygame.Rect(
                                            int((self.relative_rect.width)/2 - TILE_DISPLAY_WIDTH/2 - (EXIT_BUTTON_WIDTH - TILE_DISPLAY_WIDTH)/2 + direction.x * (EXIT_BUTTON_WIDTH + WIDTH_PADDING)), 
                                            int(self.GetCenterTileDisplayY() + (TILE_DISPLAY_HEIGHT-TEXT_HEIGHT)/2 + direction.y * (TEXT_HEIGHT + HEIGHT_PADDING*2)),
                                            int(EXIT_BUTTON_WIDTH),
                                            int(TEXT_HEIGHT)
                                        ),
                                        manager=self.ui_manager,
                                        container=self,
                                        parent_element=self,
                                        object_id="exitbutton",
                                        text="Exit Open"
                                    )

    def GetDesiredHeight(self):
        return self.editButtons[tileset.Dir(0,1)].relative_rect.bottom + HEIGHT_PADDING

    def LoadImage(self, tile):
        loadedImage = self.imageLoader.LoadOrGetImage(tile.GetFile())
        tileSurface = pygame.Surface((int(self.tileSize.x), int(self.tileSize.y)))
        tileSurface.blit(loadedImage, (0,0), (tile.GetPos().x, tile.GetPos().y, self.tileSize.x, self.tileSize.y))
        return tileSurface
    
    def SetTile(self, tile, tileset):
        self.tile = tile
        self.tileset = tileset
        self.tileSize = tileset.GetTileSize()
        tileSurface = self.LoadImage(tile)
        self.SetImage(tileSurface)

        for dir in DIRS:
            self.UpdateWalkability(self.tile, dir)

    def GetCenterTileDisplayY(self):
        return 2*TILE_DISPLAY_HEIGHT + HEIGHT_PADDING
    
    def SetImage(self, loadedImage):
        if self.tileImage:
            self.tileImage.kill()

        self.tileImage = UIImage( 
                                    relative_rect=pygame.Rect(
                                        int(self.relative_rect.width/2 - TILE_DISPLAY_WIDTH/2), 
                                        int(self.GetCenterTileDisplayY()), 
                                        int(TILE_DISPLAY_WIDTH), 
                                        int(TILE_DISPLAY_HEIGHT)
                                    ),
                                    image_surface=loadedImage,
                                    manager=self.ui_manager, 
                                    container=self, 
                                    parent_element=self,
                                    object_id="tileimage"
                                )

    def UpdateWalkability(self, tile, dir):
        if tile.GetName() != self.tile.GetName():
            return
        
        if tile.CanExit(dir):
            self.editButtons[dir].select()
        else:
            self.editButtons[dir].unselect()

    def RunSetWalkabilityCommand(self, tile, dir, walkable):
        if tile.CanExit(dir) == walkable:
            return
        
        pub.sendMessage("tileseteditor.setwalkability", tile=tile, dir=dir, walkable=walkable)
