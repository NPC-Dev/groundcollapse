import pygame
from pygame_gui.elements.ui_panel import UIPanel
from pygame_gui.elements.ui_label import UILabel
from pygame_gui.elements.ui_button import UIButton
from pygame_gui.elements.ui_image import UIImage
from pygame_gui.elements.ui_text_entry_line import UITextEntryLine
from groundcollapse import tileset
from groundcollapse.editor.commands import settextcommand
from pubsub import pub
import pygame_gui

WIDTH_PADDING=25
HEIGHT_PADDING=20
TEXT_HEIGHT=25
TILE_DISPLAY_WIDTH=32
TILE_DISPLAY_HEIGHT=32
EDIT_BUTTON_WIDTH=100
DELETE_BUTTON_WIDTH = 100
DUPLICATE_BUTTON_WIDTH = 100

ERROR_COLOR = (255,0,0)
CHANGED_TEXT_COLOR = (255, 255, 180)

class TileDetailPanel(UIPanel):
    def __init__(self, 
                rect, 
                manager, 
                imageLoader,
                element_id="tiledetail",
                container=None,
                tile=None,
                tileset=None
                ):
        UIPanel.__init__(self, relative_rect=rect, starting_layer_height=1, manager=manager, container=container, element_id=element_id)

        self.imageLoader = imageLoader
        self.tileSize = tileset.GetTileSize()
        self.tileset = tileset

        self.tileNameLabel = UILabel(
                                    relative_rect=pygame.Rect(
                                        int(WIDTH_PADDING), 
                                        int(HEIGHT_PADDING), 
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)), 
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Name", 
                                    manager=manager, 
                                    container=self
                                )
         
        self.tileNameBox = UITextEntryLine(
                                    relative_rect=pygame.Rect(
                                        int(self.tileNameLabel.relative_rect.right + WIDTH_PADDING),
                                        int(self.tileNameLabel.relative_rect.top),
                                        int(self.rect.width - self.tileNameLabel.relative_rect.right - (2 * WIDTH_PADDING)),
                                        int(TEXT_HEIGHT)
                                    ), 
                                    manager=manager, 
                                    container=self, 
                                    parent_element=self, 
                                    object_id="tilename"
                                )
        

        self.tileImagePathLabel = UILabel(
                                    relative_rect=pygame.Rect(
                                        int(WIDTH_PADDING), 
                                        int(self.tileNameLabel.relative_rect.bottom + HEIGHT_PADDING), 
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)), 
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Image Path", 
                                    manager=manager, 
                                    container=self
                                )
        
        self.tileImagePathButton = UIButton(
                                    relative_rect=pygame.Rect(
                                        int(self.tileImagePathLabel.relative_rect.right + WIDTH_PADDING),
                                        int(self.tileImagePathLabel.relative_rect.top),
                                        int(EDIT_BUTTON_WIDTH),
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Pick",
                                    manager=manager, 
                                    container=self, 
                                    parent_element=self, 
                                    object_id="imagepathbutton"
                                )

                                    
        self.tileImagePathBox = UITextEntryLine(
                                    relative_rect=pygame.Rect(
                                        int(self.tileImagePathButton.relative_rect.right + WIDTH_PADDING),
                                        int(self.tileImagePathButton.relative_rect.top),
                                        int(self.rect.width - self.tileImagePathButton.relative_rect.right - (2 * WIDTH_PADDING)),
                                        int(TEXT_HEIGHT)
                                    ), 
                                    manager=manager, 
                                    container=self, 
                                    parent_element=self, 
                                    object_id="imagepath"
                                )

        self.defaultTextColor = self.tileImagePathBox.text_colour
        
        self.tileXLabel = UILabel(
                                    relative_rect=pygame.Rect(
                                        int(WIDTH_PADDING), 
                                        int(self.tileImagePathBox.relative_rect.bottom + HEIGHT_PADDING), 
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)), 
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="X", 
                                    manager=manager, 
                                    container=self
                                )
                                
        self.tileXTextBox = UITextEntryLine(
                                    relative_rect=pygame.Rect(
                                        int(self.tileXLabel.relative_rect.right + WIDTH_PADDING),
                                        int(self.tileXLabel.relative_rect.top),
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)),
                                        int(TEXT_HEIGHT)
                                    ), 
                                    manager=manager, 
                                    container=self,
                                    parent_element=self, 
                                    object_id="tilexbox"
                                )

        self.tileYLabel = UILabel(
                                    relative_rect=pygame.Rect(
                                        int(self.tileXTextBox.relative_rect.right + WIDTH_PADDING), 
                                        int(self.tileImagePathBox.relative_rect.bottom + HEIGHT_PADDING), 
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)), 
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Y", 
                                    manager=manager, 
                                    container=self
                                )
                                
        self.tileYTextBox = UITextEntryLine(
                                    relative_rect=pygame.Rect(
                                        int(self.tileYLabel.relative_rect.right + WIDTH_PADDING),
                                        int(self.tileYLabel.relative_rect.top),
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)),
                                        int(TEXT_HEIGHT)
                                    ), 
                                    manager=manager, 
                                    container=self, 
                                    parent_element=self, 
                                    object_id="tileybox"
                                )
        
        self.tileImageLabel = UILabel(
                                    relative_rect=pygame.Rect(
                                        int(WIDTH_PADDING), 
                                        int(self.tileYTextBox.relative_rect.bottom + HEIGHT_PADDING), 
                                        int(self.rect.width/4 - (2 * WIDTH_PADDING)), 
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Image", 
                                    manager=manager, 
                                    container=self
                                )
                                
        self.tileEditButton = UIButton(
                                    relative_rect=pygame.Rect(
                                        int(self.rect.width - EDIT_BUTTON_WIDTH - WIDTH_PADDING), 
                                        int(self.tileYTextBox.relative_rect.bottom + HEIGHT_PADDING), 
                                        int(EDIT_BUTTON_WIDTH), 
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Edit Rules", 
                                    manager=manager, 
                                    container=self,
                                    object_id="edittilesettings"
                                )

        self.deleteButton = UIButton(
                                    relative_rect=pygame.Rect(
                                        int(self.tileEditButton.relative_rect.left - DELETE_BUTTON_WIDTH - WIDTH_PADDING), 
                                        int(self.tileEditButton.relative_rect.top), 
                                        int(DELETE_BUTTON_WIDTH), 
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Delete", 
                                    manager=manager, 
                                    container=self,
                                    object_id="deletetile"
                                )

        self.duplicateButton = UIButton(
                                    relative_rect=pygame.Rect(
                                        int(self.deleteButton.relative_rect.left - DUPLICATE_BUTTON_WIDTH - WIDTH_PADDING), 
                                        int(self.deleteButton.relative_rect.top), 
                                        int(DUPLICATE_BUTTON_WIDTH), 
                                        int(TEXT_HEIGHT)
                                    ), 
                                    text="Duplicate", 
                                    manager=manager, 
                                    container=self,
                                    object_id="duplicatetile"
                                )

        self.tileImage = None

        if None != tile:
            self.SetTile(tile, tileset)
    
    def process_event(self, event):
        handled = super().process_event(event)
        if event.type != pygame.USEREVENT:
            return False

        if event.user_type == pygame_gui.UI_BUTTON_PRESSED:
            if event.ui_element == self.tileEditButton:
                pub.sendMessage("tileseteditor.edit_tile_settings", tile=self.GetTile())
            elif event.ui_element == self.tileImagePathButton:
                pub.sendMessage("tileseteditor.pick_file", 
                                initialPath = self.tile.GetFile(), 
                                filePickedEvent=self.ChangeTileImagePath, 
                                allow_existing_only=True, 
                                allow_dirs=False)
            elif event.ui_element == self.deleteButton:
                pub.sendMessage("tileseteditor.delete_tile", tile=self.GetTile())
            elif event.ui_element == self.duplicateButton:
                pub.sendMessage("tileseteditor.duplicate_tile", tile=self.GetTile())

        elif event.user_type == pygame_gui.UI_TEXT_ENTRY_CHANGED:
            self.HandleTextChange(event)

        elif event.user_type == pygame_gui.UI_TEXT_ENTRY_FINISHED:
            self.HandleTextEntry(event)
    
    def HandleTextChange(self, event):
        if event.ui_element == self.tileImagePathBox or \
            event.ui_element == self.tileXTextBox or \
            event.ui_element == self.tileYTextBox or \
            event.ui_element == self.tileNameBox:
            event.ui_element.text_colour = CHANGED_TEXT_COLOR

    def HandleTextEntry(self, event):
        if event.ui_element == self.tileImagePathBox or \
            event.ui_element == self.tileXTextBox or \
            event.ui_element == self.tileYTextBox or \
            event.ui_element == self.tileNameBox:
            event.ui_element.text_colour = self.defaultTextColor

        if event.ui_element == self.tileImagePathBox:
            self.RunChangeTilePathCommand(event.text)
        elif event.ui_element == self.tileXTextBox:
            self.RunChangeTileXCommand(event.text)
        elif event.ui_element == self.tileYTextBox:
            self.RunChangeTileYCommand(event.text)
        elif event.ui_element == self.tileNameBox:
            self.RunChangeTileNameCommand(event.text)

    def RunChangeTilePathCommand(self, text):
        if (self.tile.GetFile() == text):
            # Don't bother running command if it won't do anything
            return

        command = settextcommand.SetTextCommand(self.tile.GetFile(), text, self.ChangeTileImagePath)
        pub.sendMessage("tileseteditor.run", command=command)
    
    def RunChangeTileNameCommand(self, text):
        if (self.tile.GetName() == text):
            # Don't bother running command if it won't do anything
            return

        command = settextcommand.SetTextCommand(self.tile.GetName(), text, self.ChangeTileName)
        pub.sendMessage("tileseteditor.run", command=command)
    
    def RunChangeTileXCommand(self, text):
        oldText = str(self.tile.GetPos().x)
        if (oldText == text):
            # Don't bother running command if it won't do anything
            return

        command = settextcommand.SetTextCommand(oldText, text, self.ChangeTileX)
        pub.sendMessage("tileseteditor.run", command=command)

    def RunChangeTileYCommand(self, text):
        oldText = str(self.tile.GetPos().y)
        if (oldText == text):
            # Don't bother running command if it won't do anything
            return

        command = settextcommand.SetTextCommand(oldText, text, self.ChangeTileY)
        pub.sendMessage("tileseteditor.run", command=command)

    def GetTile(self):
        return self.tile

    def GetDesiredHeight(self):
        if not self.tileImage:
            return self.tileImageLabel.relative_rect.bottom + HEIGHT_PADDING

        return self.tileImage.relative_rect.bottom + HEIGHT_PADDING

    def SetTile(self, tile, tileset):
        self.tile = tile
        self.tileNameBox.set_text(tile.GetName())
       
        self.OnTileImageChanged()

        tilePos = tile.GetPos()
        self.tileXTextBox.set_text(str(tilePos.x))
        self.tileYTextBox.set_text(str(tilePos.y))
    
    def ChangeTileName(self, name):
        oldName = self.tile.GetName()

        self.tileNameBox.set_text(name)

        if oldName == name:
            self.tileNameBox.text_colour = self.defaultTextColor
            return True

        if not self.tileset.RenameTile(self.tile, oldName, name):
            self.tileNameBox.text_colour = ERROR_COLOR
            return False
        else:
            self.tileNameBox.text_colour = self.defaultTextColor
            return True

    def ChangeTileImagePath(self, path):
        self.tile.SetFile(path)
        self.OnTileImageChanged()
    
    def OnTileImageChanged(self):
        self.tileImagePathBox.set_text(self.tile.GetFile())
        tileSurface = self.LoadImage(self.tile)
        self.SetImage(tileSurface)

    def ChangeTileX(self, x):
        try:
            self.tileXTextBox.set_text(x)
            self.tile.SetPos(int(x), self.tile.GetPos().y)
            self.tileXTextBox.text_colour = self.defaultTextColor
            self.OnTileImageChanged()
        except ValueError:
            self.tileXTextBox.text_colour = ERROR_COLOR
            return

    def ChangeTileY(self, y):
        try:
            self.tileYTextBox.set_text(y)
            self.tile.SetPos(self.tile.GetPos().x, int(y))
            self.tileYTextBox.text_colour = self.defaultTextColor
            self.OnTileImageChanged()
        except ValueError:
            self.tileYTextBox.text_colour = ERROR_COLOR
            return

    def LoadImage(self, tile):
        loadedImage = self.imageLoader.LoadOrGetImage(tile.GetFile())
        tileSurface = pygame.Surface((int(self.tileSize.x), int(self.tileSize.y)))
        if None == loadedImage:
            return tileSurface
        tileSurface.blit(loadedImage, (0,0), (tile.GetPos().x, tile.GetPos().y, self.tileSize.x, self.tileSize.y))
        return tileSurface

    def SetImage(self, loadedImage):
        if self.tileImage:
            self.tileImage.kill()

        self.tileImage = UIImage( 
                                    relative_rect=pygame.Rect(
                                        int(self.tileImageLabel.relative_rect.right + WIDTH_PADDING),
                                        int(self.tileYLabel.relative_rect.bottom + HEIGHT_PADDING),
                                        int(TILE_DISPLAY_WIDTH),
                                        int(TILE_DISPLAY_HEIGHT)
                                    ),
                                    image_surface=loadedImage,
                                    manager=self.ui_manager, 
                                    container=self, 
                                    parent_element=self,
                                    object_id="tileimage"
                                )
