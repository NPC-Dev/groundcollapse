#! /usr/bin/python

from . import tileset
import heapq
import functools
import pygame
from typing import Any

UNCONSTRAINED_COST_MULTIPLIER = 100

@functools.total_ordering
class PrioritizedChunk:
    def __init__(self, x, y, cameraHolder, levelStore, chunkSize):
        self.pos = tileset.Pos(x,y)
        
        self.DetermineNeighborLocs(chunkSize)

        centerX = x + chunkSize.x/2.0
        centerY = y + chunkSize.y/2.0
        self.center = tileset.Pos(centerX,centerY)
       
        self.rect = pygame.Rect(self.pos.x, self.pos.y, chunkSize.x, chunkSize.y)

        self.cameraHolder = cameraHolder
        self.levelStore = levelStore

        self.UpdatePriority()
    
    def DetermineNeighborLocs(self, chunkSize):
        self.leftNeighbor = tileset.Pos(self.pos.x - chunkSize.x, self.pos.y)
        self.rightNeighbor = tileset.Pos(self.pos.x + chunkSize.x, self.pos.y)
        self.bottomNeighbor = tileset.Pos(self.pos.x, self.pos.y - chunkSize.y)
        self.topNeighbor = tileset.Pos(self.pos.x, self.pos.y + chunkSize.y)

    def CheckIfUnconstrained(self):
        return not (self.levelStore.ContainsAt(self.leftNeighbor) \
                or self.levelStore.ContainsAt(self.rightNeighbor) \
                or self.levelStore.ContainsAt(self.topNeighbor) \
                or self.levelStore.ContainsAt(self.bottomNeighbor)) 

    def UpdatePriority(self):
        self.distCost = self.DistCost()
        if UNCONSTRAINED_COST_MULTIPLIER > 1:
            self.isUnconstrained = self.CheckIfUnconstrained()
            if self.isUnconstrained:
                self.distCost *= UNCONSTRAINED_COST_MULTIPLIER

    def __lt__(self, other):
        return self.distCost < other.distCost

    def DistCost(self):
        cameraPos = self.cameraHolder.GetCameraCenter()
        xOffset = (self.center.x - cameraPos.x)
        yOffset = (self.center.y - cameraPos.y)
        distSqr = xOffset * xOffset + yOffset * yOffset
        return distSqr

class BuildQueue:
    def __init__(self, cameraHolder, levelStore, chunkSize):
        self.queue = []
        self.SetCameraHolder(cameraHolder)
        self.levelStore = levelStore
        self.chunkSize = chunkSize

    def SetCameraHolder(self, cameraHolder):
        self.cameraHolder = cameraHolder

    def Clear(self):
        self.queue.clear()

    def AddItem(self, item):
        queueItem = PrioritizedChunk(item.x, item.y, self, self.levelStore, self.chunkSize)
        heapq.heappush(self.queue, queueItem)

    def OnUpdatesFinished(self):
        for item in self.queue:
            item.UpdatePriority()

        heapq.heapify(self.queue)

    def Pop(self):
        queueItem = heapq.heappop(self.queue)
        return queueItem.pos

    def GetCameraRect(self):
        return self.cameraHolder.GetCameraRect()

    def GetCameraCenter(self):
        return self.cameraHolder.GetCameraCenter()

    def Size(self):
        return len(self.queue)
