# GroundCollapse
### A library, a tool, and a demo for creating infinite generative, tiled, 2D terrain or game levels. It uses constraint solving (with clingo), inspired by Wave Function Collapse.

## by NPCDev

## https://npc.codes

### License Information:

See LICENSE.md for primary license info. Note that some assets are licensed separately, see Credits below for details.

Notwithstanding this open-source license, we strongly condemn any use of this
software to harm individuals or groups. Such usage would include all military
use, use that violates privacy, use that discriminates against marginalized or
underprivileged groups or individuals, and more. We will work to oppose and
stop any use of this software for such purposes.

## Pre-packaged Builds

If you downloaded the packaged version of GroundCollapse (such as from https://npcdev.itch.io/groundcollapse), this section is for you.

### Installing:

You should not need to install anything. You can just copy the GroundCollapse folder to your favorite place (such as your Home folder, or C:\Program Files on Windows).

### Running:

#### Windows:

Run groundcollapse.bat or groundcollapse.exe to run the editor, which you can use to create a tileset and/or generate levels based on the tileset.

If you want to provide extra options, such as setting up your own tileset, you can edit groundcollapse.bat or provide extra command line options to groundcollapse.exe.

See the section "Options," below, for the options available.

In the .bat file, you can add to the options to the line:
~~~~
start "" /wait /B /D".\groundcollapse" "groundcollapse.exe"
~~~~

For example, you could use the following to see the "help" message:
~~~~
start "" /wait /B /D".\groundcollapse" "groundcollapse.exe" --help
~~~~

#### Linux:

Make sure the groundcollapse program file is executable, and then run it in your desktop environment or from a terminal.

You can pass the below options to change how GroundCollapse operates, such as specifying your own tileset.

### Options:

The available options to pass to GroundCollapse are:
~~~~
-h, --help
--path [PATH]
--program [PROGRAM]
--tileset [TILESET]
--seed [SEED]
--size [SIZE]
--display_width [DISPLAY_WIDTH]
--display_height [DISPLAY_HEIGHT]
--scale_width [SCALE_WIDTH]
--scale_height [SCALE_HEIGHT]
--rand_freq [RAND_FREQ]
--cores [CORES]
--failureMillis [FAILUREMILLIS]
--fallbackOptimizeMillis [FALLBACKOPTIMIZEMILLIS]
--debugOnFallback [DEBUGONFALLBACK]
--worldBuildScreens [WORLDBUILDSCREENS]
--maxLoadedScreens [MAXLOADEDSCREENS]
--optimizationMillis [OPTIMIZATIONMILLIS]
--optimizationScreens [OPTIMIZATIONSCREENS]
--saveChunks [SAVECHUNKS]
--saveHumanReadableChunks [SAVEHUMANREADABLECHUNKS]
--baseChunkPath [BASECHUNKPATH]
--levelName [LEVELNAME]
--makeUniqueLevelID [MAKEUNIQUELEVELID]
--chunkNamePrefix [CHUNKNAMEPREFIX]
--aggregateStats [AGGREGATESTATS]
~~~~

## Source Builds

If building GroundCollapse from source, you should follow the below instructions:

### Installing:

- Install miniconda (https://docs.conda.io/en/latest/miniconda.html)

- conda update conda

- conda create -n groundcollapse python=3.7

- conda activate groundcollapse

- conda install -c potassco clingo pandas

- conda install -c conda-forge pypubsub

- python3 -m pip install -U pygame pygame_gui
	(on some systems, this may be just "python install")


### Running:

You can run the following command to test the basic functionality:

python ./main.py

Add --help to see the many command line options that can change the program's behavior!


### Packaging:

 - conda install pyinstaller

 - pyinstaller groundcollapse.spec


### Credits:

Outdoor Tileset by Calciumtrice, licensed under Creative Commons Attribution 3.0 license. (https://opengameart.org/content/outdoor-tileset)

Megrim font by Daniel Johnson (<il.basso.buffo@gmail.com>), copyright 2011, licensed under the SIL OPEN FONT LICENSE Version 1.1

FiraCode font by The Fira Code Project Authors, copyright 2014, licensed under the SIL OPEN FONT LICENSE Version 1.1

pygame_gui default theme by Dan Lawrence, copyright (c) 2018, licensed under the MIT License

See the respective distribution directories for the details of these licenses.

Some code based on the following sources:

Nelson, M. J., & Smith, A. M. (2016). ASP with applications to mazes and levels. In Procedural Content Generation in Games (pp. 143–157). Springer.

Karth, I., & Smith, A. M. (2017). WaveFunctionCollapse is constraint solving in the wild. Proceedings of the 12th International Conference on the Foundations of Digital Games, 68. ACM.

