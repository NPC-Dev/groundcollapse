#program main(width).

%%%%%%%% RESULTS: add show annotations here to return specific resulting values

% t((X,Y),P) is the final, assigned tile pattern P at a given X, Y coordinate. 
#show t/2.

%%%%%%%% All legal adjacencies must be symmetric, or they would not make sense.

legal((DX, DY), P1, P2) :- legal((IDX, IDY), P2, P1), IDX == -DX, IDY == -DY.

%%%%%%%% Basic level definition: based on Nelson, M. J., & Smith, A. M. (2016). ASP with applications to mazes and levels. In Procedural Content Generation in Games (pp. 143–157). Springer.

param("width",width).
dim(1..width).
cell(C) :- (X,Y) == C, dim(X), dim(Y).
dir(-1..1).

adj((X1,Y1),(X2,Y2)) :-
	cell((X1,Y1)),
	cell((X2,Y2)),
	|X1-X2| == 1,
	|Y1-Y2| == 1.

adj((X1,Y1),(X2,Y2)) :-
	cell((X1,Y1)),
	cell((X2,Y2)),
	|X1-X2| == 1,
	Y1 == Y2.

adj((X1,Y1),(X2,Y2)) :-
	cell((X1,Y1)),
	cell((X2,Y2)),
	X1 == X1,
	|Y1-Y2| == 1.

% Threshold: radius of 6 tiles for being near.
near((X1,Y1),(X2,Y2)) :-
	cell((X1,Y1)),
	cell((X2,Y2)),
	(X1-X2)*(X1-X2) + (Y1-Y2)*(Y1-Y2) < 36,
	not X1 == X2,
	not Y1 == Y2.

adj((X1,Y1),(X2,Y2),(DX,DY)) :-
	cell((X1,Y1)),
	cell((X2,Y2)),
	X2-X1 == DX,
	Y2-Y1 == DY,
	|DX| == 1,
	DY == 0.

adj((X1,Y1),(X2,Y2),(DX,DY)) :-
	cell((X1,Y1)),
	cell((X2,Y2)),
	X2-X1 == DX,
	Y2-Y1 == DY,
	|DY| == 1,
	DX == 0.

% Allow certain cells to be forced to a particular pattern, and others must work around that.
t(C,P) :- pattern(P), force(C,P).

%%%%%%%%% WFC in ASP implementation: based on Karth, I., & Smith, A. M. (2017). WaveFunctionCollapse is constraint solving in the wild. Proceedings of the 12th International Conference on the Foundations of Digital Games, 68. ACM.

1 { t(C,P) : pattern(P) } 1 :- cell(C), not debugMode.

% Debug mode version, where we are allowed to have incomplete tiles.
0 { t(C,P) : pattern(P) } 1 :- cell(C), debugMode.

% Require all adjacent tiles to still have valid assignments after we assign the current tile.
:- adj(C, C2, Dir),
	t(C, P1),
	not 1 { t(C2, P2) : legal(Dir, P1, P2) },
	not allowImperfect,
	not debugMode.

% Debug mode version, where we need to enforce legality of selected adjacent tiles.

:- adj(C1, C2, (DX,DY)),
	t(C1,P1),
	t(C2,P2),
	not legal((DX,DY),P1,P2),
	debugMode.

% Cells that must be walkable must be walkable.
:- must_be_walkable(C), cell(C), t(C,P), pattern(P), not walkable(P), not allowImperfect.

% Cells that must have a particular exit.
:- require_exit(C,Dir), cell(C), t(C,P), pattern(P), not can_exit(P,Dir), not allowImperfect.

%%%%%%%%% Evaluation/optimization functions:

number(P1,R) :- R = { t(_,P1) }, pattern(P1).

%%%% Currently used:

% Minimize the number of each tile in general.

#minimize { R@1 : number(P1,R),pattern(P1) }.

% Maximize desired walkability.
#maximize { 1@10 : t(C,P), desired_walkable(C), cell(C), pattern(P), walkable(P) }.
% If allowing imperfect, maximize walkability for tiles that were supposed to definitely be walkable.
#maximize { 1@10 : t(C,P), must_be_walkable(C), cell(C), pattern(P), walkable(P), allowImperfect }.

% Maximize desired exits
#maximize { 1@10 : t(C,P), desire_exit(C,Dir), cell(C), pattern(P), can_exit(P,Dir)}.
% If allowing imperfect, maximize exit desires for tiles that were supposed to definitely have that exit.
#maximize { 1@10 : t(C,P), require_exit(C,Dir), cell(C), pattern(P), can_exit(P,Dir), allowImperfect }.

% Make debug and fallback modes optimize to look as good as possible:
assignedDebug(C,P) :- t(C,P), cell(C), pattern(P), debugMode.
#maximize { 1@2,cell(C) : assignedDebug(C,P), debugMode }.

assignedIllegal(C,P) :- t(C,P), t(C2,P2), adj(C,C2,Dir), not legal(Dir,P,P2), allowImperfect.
#minimize { 1@3,cell(C) : assignedIllegal(C,P), pattern(P), allowImperfect }.

% Minimize undesirable assignments at given strength.
#minimize { 1@10 : t(C,P), undesirable(C,P) }.

%%%% Not currently used:

% Minimized unused tile types.
%unused(P1) :- number(P1,C), C == 0, pattern(P1).

%#minimize{ 1@1 : unused(P1) }.

% Minimize the same tile being used near itself.
%#minimize { 1@1,cell(C) : near(C,C2),cell(C),cell(C2),t(C,P1),t(C2,P1) }. 

% Minimize the same tile being used next to itself.
%#minimize { 1@1,cell(C) : adj(C,C2),cell(C),cell(C2),t(C,P1),t(C,P1) }. 

%%%%%%%% Externals

#external allowImperfect.
#external debugMode.

#external force(C,P) : cell(C), pattern(P).            

#external cell(C) : (X,Y) == C, X == 0, dim(Y).
#external cell(C) : (X,Y) == C, Y == 0, dim(X).
#external cell(C) : (X,Y) == C, X == width+1, dim(Y).
#external cell(C) : (X,Y) == C, Y == width+1, dim(X).

#external must_be_walkable(C) : cell(C).
#external desired_walkable(C) : cell(C).
#external require_exit(C,Dir) : (DX,DY) == Dir, cell(C), dir(DX), dir(DY).
#external desire_exit(C,Dir) : (DX,DY) == Dir, cell(C), dir(DX), dir(DY).

#external undesirable(C,P) : cell(C), pattern(P).
