#! /usr/bin/python

import os
import pygame
import sys
import argparse
from groundcollapse import tilesetloader
from groundcollapse import levelbuilder
from groundcollapse import levelviewer
from groundcollapse import levelstore
from groundcollapse import tileset
from groundcollapse import chunkwriter
from groundcollapse import cameraholder
from groundcollapse import infinitegroundgenerator
from groundcollapse.editor import editor

if __name__ == '__main__':
    if getattr(sys, 'frozen', False):
        os.chdir(sys._MEIPASS)

    argparser = argparse.ArgumentParser()
    argparser.add_argument('--path', nargs="?", const="wfc.pl", type=str, default="wfc.pl")
    argparser.add_argument('--program', nargs='?', const="main", type=str, default="main")
    argparser.add_argument('--tileset', nargs='?', const="", type=str, default="tilesets/calciumtrice-outdoor/simple-tileset-purple.json")
    argparser.add_argument('--seed', nargs='?', const=-1, type=int, default=-1)
    argparser.add_argument('--size', nargs='?', const=5, type=int, default=5)
    argparser.add_argument('--display_width', nargs='?', const=1280, type=int, default=1280)
    argparser.add_argument('--display_height', nargs='?', const=768, type=int, default=768)
    argparser.add_argument('--scale_width', nargs='?', const=2, type=float, default=2)
    argparser.add_argument('--scale_height', nargs='?', const=2, type=float, default=2)
    argparser.add_argument('--rand_freq', nargs='?', const=1.0, type=float, default=1.0)
    argparser.add_argument('--cores', nargs='?', const=-1, type=int, default=-1)
    argparser.add_argument('--failureMillis', nargs='?', const=3000, type=int, default=3000)
    argparser.add_argument('--fallbackOptimizeMillis', nargs='?', const=500, type=int, default=500)
    argparser.add_argument('--debugOnFallback', nargs='?', const=True, type=bool, default=False)
    argparser.add_argument('--worldBuildScreens', nargs='?', const=4.0, type=float, default=4.0)
    argparser.add_argument('--maxLoadedScreens', nargs='?', const=8.0, type=float, default=8.0)
    argparser.add_argument('--optimizationMillis', nargs='?', const=200, type=int, default=200)
    argparser.add_argument('--optimizationScreens', nargs='?', const=2.0, type=float, default=2.0)
    argparser.add_argument('--saveChunks', nargs="?", const=True, type=bool, default=False)
    argparser.add_argument('--saveHumanReadableChunks', nargs="?", const=True, type=bool, default=False)
    argparser.add_argument('--baseChunkPath', nargs="?", const="chunks/", type=str, default="chunks/")
    argparser.add_argument('--levelName', nargs="?", const="mainlevel", type=str, default="mainlevel")
    argparser.add_argument('--makeUniqueLevelID', nargs="?", const=True, type=bool, default=False)
    argparser.add_argument('--chunkNamePrefix', nargs="?", const="chunk", type=str, default="chunk")
    argparser.add_argument('--aggregateStats', nargs="?", const=False, type=bool, default=False)

    args = argparser.parse_args()

    tilesetLoader = tilesetloader.TilesetLoader()
    tiles = None
    if args.tileset != None and len(args.tileset) > 0:
        tiles = tilesetLoader.LoadTileset(args.tileset)

    if args.saveChunks or args.saveHumanReadableChunks:
        print("Making a chunk writer.")
        chunkWriter = chunkwriter.ChunkWriter(args.baseChunkPath + str(args.levelName), 
                                                chunkNamePrefix=args.chunkNamePrefix, 
                                                makeUniqueID=args.makeUniqueLevelID, 
                                                writePickled=args.saveChunks, 
                                                writeHumanReadable=args.saveHumanReadableChunks)

    editor = editor.Editor(tilesetLoader = tilesetLoader, 
                            display_width = args.display_width, 
                            display_height = args.display_height, 
                            scale_x = args.scale_width, 
                            scale_y = args.scale_height, 
                            tileset = tiles,
                            args=args)

    try:
        while(editor.Tick()):
            pass

    finally:
        editor.Finalize()